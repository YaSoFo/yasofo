# -*- coding: utf-8 -*-
# This source code is part of YaSoFo, distrubuted
# under the EUPL v1.2 license. See 'LICENSE' for further
# information

from numpy import genfromtxt, array, trapz, interp, arange, where, exp
import scipy.constants as phys_const
from matplotlib.pyplot import plot, xlabel, ylabel, show
import os.path
am15_fname = os.path.join(os.path.split(os.path.dirname(__file__))[0], 'am15g.dat')
water_abs_fname = os.path.join(os.path.dirname(__file__), 'water-absorption.dat')

def nm_to_ev(wavelength):
    """Convert wavelength in nm to electronvolts."""
    ev = phys_const.h*phys_const.c/(wavelength*phys_const.e*10**(-9))
    return ev


def ev_to_nm(ev):
    """Convert electronvolts to wavelength in nm."""
    wavelength = phys_const.h*phys_const.c/(ev*phys_const.e*10**(-9))
    return wavelength


class solspec(object):
    """The solar spectrum class.
    Attributes:
        filename: The filename from which the spectrum was read.
        data: An ndarray containing the spectrum itself.
        scaling: The factor by which the spectrum is scaled.
        comment: Any comments.
        length: Number of data points.
        power: The integrated power in W/m^2. For the efficiency definition.
        verbose: Print info about spectrum if True.
    """

    def __init__(self, filename=am15_fname, scaling=1., comment='None',
                 verbose=True, header=0, delimiter=None, usecols=None, res=0):
        self.filename = filename
        self.header = header
        self.delimiter = delimiter
        self.usecols = usecols
        self.res = res
        self.scaling = scaling
        self.comment = comment
        self.verbose = verbose
        self.data = self.imp_spectrum(self.filename, self.scaling,
                                      self.verbose, self.delimiter,
                                      self.header, self.usecols)
        self.length = len(self.data)
        self.power = self.integrate_power(self.data, False)
        self.total_flux = self.integrate_flux(max(self.data[:, 0]),
                                              min(self.data[:, 0]), False)
        if res != 0:
            change_spec_resolution(self, res)
            self.update()

    def plot(self):
        """Plots the spectrum as photon flux over wavelength."""
        plot(self.data[:, 0], self.data[:, 1])
        xlabel('Wavelength / nm')
        ylabel('Flux / Photons nm$^{-1}$m$^{-2}$s$^{-1}$')
        show()

    def update(self, verbose=False):
        """Recalc attributes."""
        self.length = len(self.data)
        self.power = self.integrate_power(self.data, False)
        if verbose is True:
            if abs(1 - self.power/1000) > 0.001:
                print('Warning: Integrated power: %5.3f W/m^2.' % self.power +
                      ' Expected: 1000 W/m^2. Use scaling?')
            else:
                print('Integrated power: %5.3f W/m^2.' % self.power)
            print('Standard for efficiency calculation assumes 1000 W/m^2!')
            print('Length: %i points. Recommended: 200-500.' % self.length)

    def imp_spectrum(self, filename, scaling=1.0, verbose=True, delimiter=None,
                     header=0, usecols=None):
        """Import spectrum from etaOpt-Format and convert from [W*m^-2*µm^-1]
        to [photons*m^-2*(nm*s)^-1].
        """
        try:
            spectrum_orig = genfromtxt(filename, delimiter=delimiter,
                                       skip_header=header, usecols=usecols)
        except IOError:
            print('Error: Spectrum file', filename, 'not found!')
            return None
        spectrum_new = array(spectrum_orig, copy=True)
        spectrum_new[:, 1] = spectrum_orig[:, 1]*spectrum_new[:, 0]*10**(-9)*\
            10**(-3)/(phys_const.h*phys_const.c)*scaling
        power = self.integrate_power(spectrum_new, print_output=False)
        if verbose is True and self.res == 0:
            print('Spectrum successfully imported. Length: %i points.' %
                  len(spectrum_new[:, 0]))
            if abs(1 - power/1000) > 0.001:
                print('Warning: Integrated power: %5.3f W/m^2.' % power +
                      ' Expected: 1000 W/m^2. Use scaling?')
            else:
                print('Integrated power: %5.3f W/m^2.' % power)
            print('Standard for efficiency calculation assumes 1000 W/m^2!')
            print('Length: %i points. Recommended: 200-500.' %
                  len(spectrum_new[:, 0]))
        return spectrum_new

    def integrate_flux(self, upper, lower=0, electronvolts=True, avEQE=1.):
        """Integrate photon flux between two limits.
        Output: Current in A per cm^2. External quant. eff. is assumed to be unity.

        Parameters
        ----------
        spectrum: solspec
            the spectrum
        upper: float
            Upper integration limit
        lower: float
            lower integration limit
        electronvolts: bool
            Limits in electronvolts? If False in nm.
        avEQE : float
            External quantum efficiency averaged over the whole bandgap range.
            Default: 1.0.
        """
        if lower == 0:
            lower = 0.0001  # avoid division by zero
        if electronvolts is False:
            upper_lambda = upper
            lower_lambda = lower
        else:
            lower_lambda = ev_to_nm(upper)
            upper_lambda = ev_to_nm(lower)
            if upper_lambda > self.data[-1, 0]:
                upper_lambda = self.data[-1, 0]
        upper_x_index = where(self.data[:, 0] >= upper_lambda)[0][0]
        lower_x_index = where(self.data[:, 0] > lower_lambda)[0][0]
        # Integrate photon flux by trapezoidal rule (as requested by etaopt Format)
        # Check if this is also true for your file format!
        photon_number = trapz(self.data[lower_x_index:upper_x_index, 1],
                              self.data[lower_x_index:upper_x_index, 0])
        current = avEQE*photon_number*phys_const.e/10000
        if avEQE > 1.:
            print('Warning: EQE greater 1. Multi-excition generation???')
        return current

    def integrate_power(self, spectrum, print_output=True):
        """Integrate power of the whole spectrum.

        Parameters
        ----------
        spectrum: numpy.ndarray
            The spectrum, format after internal conversion.
        """
        power = 0.0
        # reconvert spectrum to [W*m^-2*µm^-1]
        spectrum_new = array(spectrum, copy=True)
        spectrum_new[:, 1] = spectrum[:, 1]/(spectrum_new[:, 0]*10**(-9)
                                             / (phys_const.h*phys_const.c))
        for data in spectrum:
            power = trapz(spectrum_new[:, 1], spectrum_new[:, 0])
        if print_output is False:
            return power
        print('Power in W/m^2: %4.2f. Expected: 1000 for AM1.5G.' % power)
        print('Standard for efficiency calculation assumes 1000 W/m^2!')
        return()


# for convenience, import standard data from file am15g.dat (see README)
am15g = solspec(am15_fname)


def change_spec_resolution(spectrum, points):
    """Change the resolution of a given spectrum.

    Parameters
    ----------
    spectrum:
        The spectrum.
    points: integer
        Number of points for the new spectrum.
    """
    stepsize = (spectrum.data[-1, 0] - spectrum.data[0, 0])/points
    orig_length = spectrum.length
    new_x_axis = arange(spectrum.data[0, 0], spectrum.data[-1, 0], stepsize)
    new_y_axis = interp(new_x_axis, spectrum.data[:, 0],
                        spectrum.data[:, 1])
    spectrum.data = array([new_x_axis, new_y_axis]).T
    spectrum.update(verbose=True)
    print('Interpolated spectrum of original length %i to %i points.'
          % (orig_length, points))
#    return new_spectrum
    return()


def mod_spectrum(filename, spectrum=am15g, header=1):
    """Modify an existing spectrum by multiplying it with a filter from file.
    Returns modified spectrum.

    Parameters
    ----------
    filename: str
        Filename where the filter ist to be found. Filter file format:
        [wavelength, transmittance].
    spectrum: solspec
        The initial spectrum.
    header: int
        Number of header lines to ignore in filter file.
    """
    new_spec = solspec(verbose=False)
    filter_function = genfromtxt(filename, skip_header=header)
    if max(filter_function[:, 1]) > 1.0:
        print('Warning: at least one value of filter greater 1!')
    if min(filter_function[:, 1]) < 0.:
        print('Warning: at least one value of filter negative!')
    new_spec.data = array(spectrum.data, copy=True)
    transmittance = array(new_spec.data, copy=True)
    # interpolate the transmittance to the x-values of the spectrum
    transmittance[:, 1] = interp(transmittance[:, 0], filter_function[:, 0],
                                 filter_function[:, 1])
    new_spec.data[:, 1] = spectrum.data[:, 1]*transmittance[:, 1]
    return new_spec


def mod_spectrum_by_abs(filename=water_abs_fname, thickness=.2, spectrum=am15g,
                        header=7, reverse=True, return_transmittance=False):
    """Modify an existing spectrum by the absorption e.g. of an electrolyte
    by the application of Beer's law.
    For absorbance of liquid water, see e.g. Pope and Fry, Appl. Opt. 36 (1997)
    (380-700 nm) or Kou et al, Appl. Opt. 32 (1993) (667-2500 nm).
    http://omlc.org/spectra/water has a nice compilation.

    Parameters
    ----------
    filename: str
        Filename where the tabulated absorbance is to be found. Format:
        [Wavelength (nm), absorption coefficient (1/cm)]. Default: water data
        from above source.
    thickness: float
        Thickness of the absorbing layer in cm. Default: 2mm.
    spectrum: solspec
        The spectrum.
    header: int
        Header lines of input file to ignore.
    reverse: bool
        Reverse oder? Values should be ordered from lowest to highest lambda.
    """
    new_spec = solspec(verbose=False)
    new_spec.data = array(spectrum.data, copy=True)
    abs_coefficient = genfromtxt(filename, skip_header=header)
    if reverse is True:
        abs_coefficient = abs_coefficient[::-1]
    transmittance = array(abs_coefficient, copy=True)
    transmittance_interp = array(new_spec.data, copy=True)
    # apply Beer's law
    transmittance[:, 1] = exp(-abs_coefficient[:, 1]*thickness)
    # interpolate (linearly!) the values of transmittance to x-axis of spectrum
    transmittance_interp[:, 1] = interp(transmittance_interp[:, 0],
                                        transmittance[:, 0],
                                        transmittance[:, 1])
    new_spec.data[:, 1] = spectrum.data[:, 1]*transmittance_interp[:, 1]
    if return_transmittance is True:
        return new_spec, transmittance
    return new_spec
