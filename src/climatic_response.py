# -*- coding: utf-8 -*-

import pandas as pd
from numpy import trapz
import collections
import pvlib
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import scipy.constants as phys_const
import scipy.special.lambertw as lambertw
from numpy import genfromtxt, interp 
import scipy.optimize as spo
import math
import os.path
from scipy.interpolate import RegularGridInterpolator





def climatic_response_function(input_file, IV_cat_parameters, x, wanlass_paramter_list, Rs_Rp_list,
                               T_device_coeff = 0.025, cat_solar_area_ratio = 1,  I_treshold = 0.1,
                               EQE_diffuse = True, diffuse_spectra = [False], fix_T_device = [False], fix_theta = [False], 
                               reduced_output = False, I_total_input = False, thermally_coupled = True):
    
    
    """This function calculates the annual production of hydrogen based on
     a dual-junction solar cell using hourly TYM-data with spectrally resolved
     solar irradiance.
     
    Parameters
    ----------
    input_file: panda dataframe
        	File containing all TMY-data and spectra. This includes the
            hour, ambient temperature, solar zenith angle, solar azimuth angle,
            panelt til angle, panel azimuth angle, DNI, DHI, GHI, and the 
            spectrum. Please see the example in the YaSoFo documentation on
            how to create your own input file.
            
                       
    IV_cat_parameters: list of 6 floats
        Contains list of 6 floats obtained from the "T_fit_routine_electrolyzer"
        function (see below).
        Parameters: tafel_slope: effective tafel slope in V/dec,
                    distance: effective electrode distance in cm ,
                    conductivity_at_298K: reference Conductivity at 298K [S/cm]
                    slope_conductivity: slope of the T-dependence of the 
                    conductivity [S/(cm T)]
                    j0: Effective exchange current density in [A/cm²]
                    activation_energy - Effective activitaion energy in [J/Mol]
        

    x: float
        x in AlxGa(1-x)As   

    wanlass_paramter_list: list of the Wannlass parameters A01 [A/cm²/K³], 
        B01 [eV-1], beta02 [A/cm²/K^2.5] for the Si and AlGaAs absorber.
        First three values: A01, B01, beta02 for Si
        Last three values: A01, B01, beta02 for AlGaAs
    
    
    
    Rs_Rp_list: list of 4 floats
        Series and parallel resistance in [Ohm cm²] of the Si and AlGaAs 
        absorber.
        First two values: Rs and Rp of Si
        Last two values: Rs and Rp of AlGaAs

        
            
    T_device_coeff: Float
        Empirical device temperature coefficient in m²K/W. 
        Default: 0.025
        
        
    cat_solar_area_ratio: float
        Ratio of the area of the solar cells and the electrolyzers active area.
        Default: 1
        


    I_treshold: float
        Treshold for the irradiance in W/m² for which the calculations are 
        performed. If I is below this value, np.nan is returned in the
        result lists. 
        Default: 0.1
        
        

    EQE_diffuse: Boolean
        If True, the function differentiates between direct and diffuse
        irradiation based on the DNI and DHI from the input file. 
        Default: True
    
        
    
   diffuse_spectra: list of Boolean and non-obligatory panda dataframe
       If the first value in the list is set to "True", a second Panda
       Dataframe containing the hourly solar spectra for diffuse light is 
       required. These are then used to obtain the diffuse photocurrent in each 
       junction of the solar cell instead of only differentiating between 
       direct and diffuse irradiation based on the DNI and DHI (see 
       "EQE_diffuse" above). Please see the example in the YaSoFo 
       documentation on how to create your own input file for the diffuse 
       spectra.
       Default: [False]   
    


    fix_T_device: list of Boolean and non-obligatory float
        If the first value in the list is "True", a second float value is 
        required which fixes the device temperature to this value for 
        reference caluclations. 
        Default: [False]
    
    
    fix_theta: list of Boolean and non-obligatory float
        If the first value in the list is "True", a second float value is 
        required which fixes the angle of incidence 
        theta to this value for reference caluclations. 
        Default: [False]
        
    
    reduced_output: list of one Boolean and non-obligatory float
        If True, only the "hour"", "kg_H2/h/m², and "kg_H2/year/m²" are 
        returned from this function to save storage space. If False, complete 
        ordered dictionairy is returned (see below).
        Default: False
        
        
        
    I_total_input: Boolean
        If True, the total irradiance in [W/m²] used for the estimation of the 
        device operating temperature and efficiency calcualations is taken from 
        an entry from input file and is not calculated from the solar spectra. 
        This needs to be done, when the solar spectra for instance only 
        contain the intensity for wavelength > 1300 nm, which is reasonable 
        when Si bottom absorbers are used. Again, we refer the reader to the
        example modelling in the YaSoFo documentation.
        Default: False
        
        
        
    thermally_coupled: Boolean
        If True, the device operating temperature is estimated from the ambient
        temperature and the total irradiance. If False, the device operating 
        temperature is assumed to follow the ambient temperature for reference
        calculations. 
        Default: True
    
    

    Returns
    -------
     Returns an ordered dictionairy containing the following keys:
         
         

        * Hour - List of the hours, list of floats
        * T_outdoor - List of outdoor temperature [°C], list of floats
        * T_device - List of device temperature [°C], list of floats
        * Irradiance - List of Irradiance [W/m²], list of  float
        * IV_catalysts - IV curves of the catalysts, 2 lists of
                        floats: voltage [V], current density [mA/cm²]
        * IV_solar_cell - IV curves of the solar cell [mA/cm²], [V], 2 lists of
                        floats: voltage [V], current density [mA/cm²]
        * j_op - Operation current in [mA/cm²], list of floats
        * E_op - Operation potential in [V], list of floats
        * STH_eff - Solar-to-hydrogen efficiency in [%], list of floats
        * kg_H2_h_m² - kg Hydrogen per hour in [kg], list of floats
        * kg_H2_year_m² - kg Hydrogen per year in [kg], list of floats
        * APE - Avergae photon energy of each spectrum [eV], list of floats      
        * j_mismtach_abs - Absolute current mismatch in [mA/cm²], list of floats
        * j_mismtach_rel - Relative current mismatch in [V], list of floats
        * Photon_flux_integrated - Integrated photon flux of each spectrum
                                    in [mA/cm²], list of floats
        * j_sh_bottom - Short circuit current densitiy of the bottom absorber
                        in [mA/cm²], list of floats
        * j_sh_top - Short circuit current densitiy of the top absorber
                        in [mA/cm²], list of floats
        * j_sh - Short circuit current densitiy of the dual junction
                in [mA/cm²], list of floats
        * EQE_T - External Quantum Efficiency in both absorbers, 3 lists of
                        floats: Wavelength [µm], EQE AlGaAs [%], EQE Si [%]
        * PV_eff - Photoconversion efficiency of the solar cell [%], list of floats
        * FF - Fill Factor if the solar cell in [%], list of floats
        * Couple_eff - Coupling efficiency of the system [%], list of floats
        * Eg_top - Bandgap of the top absorber (AlGaAs), list of floats
        * Eg_bottom - Bandgap of the bottom absorber (Si), list of floats
        * Solar_zenith - Solar zenith angle in [°], list of floats
        * Solar_azimuth - Solar azimuth angle in [°], list of floats
        * Panel_tilt - Panelt tilt angle in [°], list of floats
        * Panel_azimuth - Panelt azimuth angle in [°], list of floats        
        * theta - Incidence angle in [°], list of floats
        * DNI - Direct normal irradiance in [W/m²], list of floats
        * DHI - Diffuse horizontal irradiance in [W/m²], list of floats
        * GHI - Global Horizontal Irradiance in [W/m²], list of floats
                                        
        
        
        
        if EQE_diffuse is set to True, the following keys are also returned:
            
        * Factor_direct - Ratio of direct and diffuse light impinging on the 
                            device, list of floats
        * EQE_T_diffuse - External Quantum Efficiency for diffuse irradiance in 
                        both absorbers, 3 lists of floats: Wavelength [µm], 
                        EQE AlGaAs [%], EQE Si [%]
        * j_sh_top_direct - Short circuit current densitiy of the top absorber
                        for direct irradiance in [mA/cm²], list of floats
        * j_sh_botttom_direct - Short circuit current densitiy of the bottom absorber
                        for direct irradiance in [mA/cm²], list of floats
        * j_sh_top_diffuse - Short circuit current densitiy of the top absorber
                        for diffuse irradiance in [mA/cm²], list of floats
        * j_sh_bottom_diffuse - Short circuit current densitiy of the bottom absorber
                        for diffuse irradiance in [mA/cm²], list of floats
        
        
        
      If reduced_output is set to True, only the following ordered 
      dictionairy only containing the following keys is returned:
          
        * Hour - List of the hours, list of floats
        * kg_H2_h_m² - kg Hydrogen per hour in [kg], list of floats
        * kg_H2_year_m² - kg Hydrogen per year in [kg], list of floats
        
            
    """

       
    df = input_file
    
    
    #Extract Temperature at each hour
    Hour_list =  np.array(list(df.index))
    T_out_list = df["Temperature [°C]"].to_numpy()
    
    #Extract relevant angels at each hour
    Solar_Zenith_Angle_list = df["Solar Zenith Angle"].to_numpy()
    Solar_Azimuth_Angle_list = df['Solar Azimuth Angle'].to_numpy()
    Panel_tilt_list = df['Panel Tilt'].to_numpy()
    Panel_azimuth_list = df['Panel Azimuth Angle'].to_numpy()
    
    #Extract DNI, DHI, GHI at each hour
    DNI_list = df["DNI"].to_numpy()
    DHI_list = df["DHI"].to_numpy()
    GHI_list = df["GHI"].to_numpy()
    
    #Load EQEs and define interpolation functions       
    x_EQE_list = np.array([0.025, 0.035, 0.075, 0.100, 0.125, 0.135, 0.175, 0.180, 0.185, 0.190, 0.195, 0.200, 0.205, 0.210, 0.215, 0.220, 0.225, 0.250, 0.275, 0.325, 0.375])
    theta_EQE_list = np.array([0.0, 0.77, 1.54, 2.31, 3.08, 3.85, 4.62, 5.39, 6.16, 6.94, 7.71, 9.27, 10.05, 10.83, 11.62, 12.4, 13.19, 13.98, 14.78, 15.57, 16.37, 17.18, 18.79, 19.61, 20.43, 21.25, 22.08, 22.91, 23.75, 24.59, 25.44, 26.29, 27.15, 28.9, 29.78, 30.67, 31.57, 32.47, 33.39, 34.32, 35.25, 36.2, 37.16, 38.13, 40.11, 41.13, 42.16, 43.2, 44.27, 45.35, 46.45, 47.58, 48.74, 49.92, 51.13, 53.65, 54.96, 56.33, 57.74, 59.21, 60.75, 62.36, 64.07, 65.89, 67.85, 69.98, 75.11, 78.48, 83.36])
    T_EQE_list = np.array([228, 233, 238, 243, 248, 253, 258, 263, 268, 273, 278, 283, 288, 293, 298, 303, 308, 313, 318, 323, 328, 333, 338, 343, 348, 353])
    wavelength_EQE_list = np.genfromtxt(r'' + str(os.path.abspath(os.path.join(os.path.dirname(__file__)))) + '/EQEs_ext/x0.025/eqe1_theta00.00_x0.025_T273.dat', usecols=0)

    theta_string_list = ['00.00', '00.77', '01.54', '02.31', '03.08', '03.85', '04.62', '05.39', '06.16', '06.94', '07.71', '09.27',
                         '10.05', '10.83', '11.62', '12.40', '13.19', '13.98', '14.78', '15.57', '16.37', '17.18', '18.79', '19.61', 
                         '20.43', '21.25', '22.08', '22.91', '23.75', '24.59', '25.44', '26.29', '27.15', '28.90', '29.78', '30.67', 
                         '31.57', '32.47', '33.39', '34.32', '35.25', '36.20', '37.16', '38.13', '40.11', '41.13', '42.16', '43.20', 
                         '44.27', '45.35', '46.45', '47.58', '48.74', '49.92', '51.13', '53.65', '54.96', '56.33', '57.74', '59.21', 
                         '60.75', '62.36', '64.07', '65.89', '67.85', '69.98', '75.11', '78.48', '83.36']
   
       
    Data_AlGaAs = []

    for x_ in x_EQE_list:
        os.chdir(str(os.path.abspath(os.path.join(os.path.dirname(__file__)))) + '/EQEs_ext/x{0}'.format("{:.3f}".format(x_)))
        x1_unsplit = []
        for th in theta_string_list:    
            for T in T_EQE_list:
                EQE = np.genfromtxt(r'' + str(os.path.abspath(os.path.join(os.path.dirname(__file__)))) + '/EQEs_ext/x'+str("{:.3f}".format(x_))+'/eqe1_theta'+th+'_x'+str("{:.3f}".format(x_))+'_T'+str(T)+'.dat', usecols=1)
                EQE[70:] = [0 for aa in EQE[70:]]
                x1_unsplit.append(EQE)
        x1 = np.array_split(x1_unsplit, len(theta_string_list))
        Data_AlGaAs.append(x1)
        
    EQE_int4d_AlGaAs = RegularGridInterpolator((x_EQE_list, theta_EQE_list, T_EQE_list, wavelength_EQE_list), Data_AlGaAs)




    Data_Si = []

    for x_ in x_EQE_list:
        os.chdir(str(os.path.abspath(os.path.join(os.path.dirname(__file__)))) + '/EQEs_ext/x{0}'.format("{:.3f}".format(x_)))
        x2_unsplit = []
        for th in theta_string_list:    
            for T in T_EQE_list:
                EQE2 = np.genfromtxt(r'' + str(os.path.abspath(os.path.join(os.path.dirname(__file__)))) + '/EQEs_ext/x'+str("{:.3f}".format(x_))+'/eqe2_theta'+th+'_x'+str("{:.3f}".format(x_))+'_T'+str(T)+'.dat', usecols=1)
                x2_unsplit.append(EQE2)
        x2 = np.array_split(x2_unsplit, len(theta_string_list))
        Data_Si.append(x2)

    EQE_int4d_Si = RegularGridInterpolator((x_EQE_list, theta_EQE_list, T_EQE_list, wavelength_EQE_list), Data_Si)


    
    def get_EQE_AlGaAs(x_f, theta_f, T_f):
        EQE = []
        for wave in wavelength_EQE_list:
            my_EQE = EQE_int4d_AlGaAs([x_f, theta_f, T_f, wave])
            EQE.append(my_EQE[0])
        return EQE


    def get_EQE_Si(x_f, theta_f, T_f):
        EQE = []
        for wave in wavelength_EQE_list:
            my_EQE = EQE_int4d_Si([x_f, theta_f, T_f, wave])
            EQE.append(my_EQE[0])
        return EQE

    def compute_ternary(AB,BC,x1,C):
    # this program takes in two binary parameters AB (x=1) and BC (x=0)
    # and computes the interpolated value at a molar fraction
    # of x using the bowing parameter C
        param=x1*AB+(1-x1)*BC-x1*(1-x1)*C
        return param



    def CalculateEgTAlGaAs(Temp,composition):
        ## GaAs
        GaAs={}
        GaAs["cohE"]=154.7*0.043 # where 0.043 is conversion factor from kcal/mol to eV
        GaAs["a"]=5.65325+(3.88e-5)*(Temp-298)
        GaAs["Eg_Gamma"]=1.519-(0.5405e-3)*(Temp)**2/(Temp+204)
        GaAs["Eg_X"]=1.981-(0.46e-3)*(Temp)**2/(Temp+204)
        GaAs["Eg_L"]=1.815-(0.605e-3)*(Temp)**2/(Temp+204)
        ## AlAs
        AlAs={}
        AlAs["cohE"]=178.9*0.043; # where 0.043 is conversion factor from kcal/mol to eV
        AlAs["a"]=5.6611+(2.9e-5)*(Temp-298)
        AlAs["Eg_Gamma"]=3.099-(0.885e-3)*(Temp)**2/(Temp+530)
        AlAs["Eg_X"]=2.24-(0.7e-3)*(Temp)**2/(Temp+530)
        AlAs["Eg_L"]=2.46-(0.605e-3)*(Temp)**2/(Temp+204)
        # x=0.23; % Eg=1.74 used as base to calcualte the concentration of Al
        ## AlGaAs
        AlGaAs={}
        x=composition
        AlGaAs["a"]=compute_ternary(AlAs["a"],GaAs["a"],x,0)
        AlGaAs["C_Gamma"]=-0.127+1.31*x # bowing coefficient
        AlGaAs["Eg_Gamma"]=compute_ternary(AlAs["Eg_Gamma"],GaAs["Eg_Gamma"],x,AlGaAs["C_Gamma"])
        AlGaAs["C_X"]=0.055
        AlGaAs["Eg_X"]=compute_ternary(AlAs["Eg_X"],GaAs["Eg_X"],x,AlGaAs["C_X"])
        AlGaAs["C_L"]=0
        AlGaAs["Eg_L"]=compute_ternary(AlAs["Eg_L"],GaAs["Eg_L"],x,AlGaAs["C_L"])
        AlGaAs["cohE"]=compute_ternary(AlAs["cohE"],GaAs["cohE"],x,0)
        Eg_T = min([AlGaAs["Eg_Gamma"], AlGaAs["Eg_X"], AlGaAs["Eg_L"]])
        return Eg_T
    

        
        
    #Extract Spectrum and Power at each hour
    wavelength = list(df.columns.values)[16:]
    wavelength_float = list(map(float, wavelength))
        
    spectrum_list = []
    
    if I_total_input == False:          
        spectrum_irradiance_list = []
        
    for i in range(len(Hour_list)):
        Spectrum_list_raw = df.iloc[i].values.tolist()   
        spectrum = np.array(Spectrum_list_raw[16:])
        spectrum_no_negative = np.where(spectrum<0, 0, spectrum)
        spectrum_list.append(spectrum_no_negative)
        
        if I_total_input == False:
            integrated_power = trapz(np.array(spectrum_no_negative), np.array(wavelength_float)) #in W/m²
            spectrum_irradiance_list.append(integrated_power)
        
    if I_total_input == True:        
        spectrum_irradiance_list = df['total_I_panel'].to_numpy()
        


    def find_nearest(array, value):
        array = np.asarray(array)
        idx = (np.abs(array - value)).argmin()
        return idx


    index_1200nm_ape = find_nearest(wavelength_float, 1.2)



    if diffuse_spectra[0] == False:

        APE_list = []
               
        for i in range(len(spectrum_list)):
            APE_zähler = np.trapz(np.array(spectrum_list[i][:index_1200nm_ape]), np.array(wavelength_float[:index_1200nm_ape])) 
            APE_nenner = (phys_const.e / (phys_const.h * phys_const.c)) * np.trapz(np.array(spectrum_list[i][:index_1200nm_ape])*np.array(wavelength_float[:index_1200nm_ape]), np.array(wavelength_float[:index_1200nm_ape]))*1e-6
            APE =  APE_zähler/APE_nenner 
            APE_list.append(APE)
            

    
    #if diffuse spectra are in a separate input file
        
    if diffuse_spectra[0] == True:
        
        spectrum_list_diffuse = []
        
        df_diffuse = diffuse_spectra[1]
        
        for i in range(len(Hour_list)):
            Spectrum_list_raw_diffuse = df_diffuse.iloc[i].values.tolist()   
            spectrum_diffuse = np.array(Spectrum_list_raw_diffuse[0:])
            spectrum_no_negative_diffuse = np.where(spectrum_diffuse<0, 0, spectrum_diffuse)
            spectrum_list_diffuse.append(spectrum_no_negative_diffuse)
            
        
        spectrum_list_sum = np.array(spectrum_list) + np.array(spectrum_list_diffuse)
        
        
        APE_list = []
               
        for i in range(len(spectrum_list_sum)):
            APE_zähler = np.trapz(np.array(spectrum_list_sum[i][:index_1200nm_ape]), np.array(wavelength_float[:index_1200nm_ape])) 
            APE_nenner = (phys_const.e / (phys_const.h * phys_const.c)) * np.trapz(np.array(spectrum_list_sum[i][:index_1200nm_ape])*np.array(wavelength_float[:index_1200nm_ape]), np.array(wavelength_float[:index_1200nm_ape]))*1e-6
            APE =  APE_zähler/APE_nenner 
            APE_list.append(APE)
            
           
    
    
    #Create all results list that will be filled while looping over each hour
    T_device_list = []
    IV_solar_cells = []
    IV_catalysts_list = []
    j_op_list = []
    E_op_list = []
    STH_eff_list = []
    kg_H2_per_hour_m2_list = []
    current_mismatch_abs_list = []
    current_mismatch_rel_list = []
    Total_integrated_photon_flux_list = []
    j_sh_top_list = []
    j_sh_bottom_list = []
    EQE_T_list = []
    EQE_T_list_diffuse = []
    PV_eff_list = []
    FF_list = []
    j_sh_list = []
    V_oc_list = []
    Couple_eff_list = []
    theta_list = []
    Eg_bottom_list = []
    Eg_top_list = []
    Factor_direct_list = []
    J_ph_AlGaAs_direct_list = []
    J_ph_AlGaAs_diffuse_list = []
    J_ph_Si_direct_list = []
    J_ph_Si_diffuse_list = []
    

    #Now start to iterate over each hour
    for i in range(len(Hour_list)):
        
        print('The ' + str(i) + '. hour' + ' is modelled')

                        
        
        #get device T
        if fix_T_device[0] == False:
            
            if spectrum_irradiance_list[i] > 0:
                T_device = T_out_list[i] + spectrum_irradiance_list[i]*T_device_coeff
            if spectrum_irradiance_list[i] <= 0:
                T_device = T_out_list[i]
                
            T_device_list.append(T_device)
        
        
        if fix_T_device[0] != False:

            T_device = fix_T_device[1]
            T_device_list.append(T_device)
            print('Device temperature is fixed')
                
            
        wavelength_mykrom = np.array(wavelength_float)
        spectrum_flux = phys_const.e* (np.array(spectrum_list[i])*wavelength_mykrom*1e-6) / (phys_const.h * phys_const.c) #A/m² #woher kommt 1e-6?
        spectrum_flux_mA_cm2 = spectrum_flux*0.1 #mA/cm²
        Total_integrated_photon_flux = np.trapz(spectrum_flux_mA_cm2, wavelength_mykrom)
        Total_integrated_photon_flux_list.append(Total_integrated_photon_flux)
        

        
        if diffuse_spectra[0] == True:
            spectrum_flux_diffuse = phys_const.e* (np.array(spectrum_list_diffuse[i])*wavelength_mykrom*1e-6) / (phys_const.h * phys_const.c) #A/m² #woher kommt 1e-6?
            spectrum_flux_mA_cm2_diffuse = spectrum_flux_diffuse*0.1 #mA/cm²
        
        
        
        if spectrum_irradiance_list[i] < I_treshold:
            

            IV_solar_cells.append(np.nan)
            IV_catalysts_list.append(np.nan)
            j_op_list.append(np.nan)
            E_op_list.append(np.nan)
            STH_eff_list.append(np.nan)
            kg_H2_per_hour_m2_list.append(0)
            current_mismatch_abs_list.append(np.nan)
            current_mismatch_rel_list.append(np.nan)
            j_sh_top_list.append(np.nan)
            j_sh_bottom_list.append(np.nan)
            EQE_T_list.append(np.nan)
            EQE_T_list_diffuse.append(np.nan)
            PV_eff_list.append(np.nan)
            FF_list.append(np.nan)
            j_sh_list.append(np.nan)
            V_oc_list.append(np.nan)
            Couple_eff_list.append(np.nan)
            theta_list.append(np.nan)
            Eg_bottom_list.append(np.nan)
            Eg_top_list.append(np.nan)
            Factor_direct_list.append(np.nan)
            J_ph_AlGaAs_direct_list.append(np.nan)
            J_ph_AlGaAs_diffuse_list.append(np.nan)
            J_ph_Si_direct_list.append(np.nan)
            J_ph_Si_diffuse_list.append(np.nan)
            
            print('No Sunshine')
            continue
        
        
                        
        Eg_bottom_device = 1.166-(4.73e-4)*((T_device + 273.15))**2/((T_device + 273.15)+636)
        Eg_bottom_list.append(Eg_bottom_device)
                            
    
        Eg_top_device = CalculateEgTAlGaAs((T_device + 273.15),x)
        Eg_top_list.append(Eg_top_device)
        
                        
        if fix_theta[0] == False:

            #get theta with pvlib library
            theta = pvlib.irradiance.aoi(Panel_tilt_list[i], Panel_azimuth_list[i], Solar_Zenith_Angle_list[i] , Solar_Azimuth_Angle_list[i])
                         
            
        if fix_theta[0] != False:
            theta = fix_theta[1]
            print('Theta is fixed')
        
        if theta <= 0:
            theta = 0

                    
        if theta >= 83.36:
            theta = 83.36

            
        
        theta_list.append(theta)    
                

        if  -45.15 <= T_device <= 79.85:
            
            my_EQE_AlGaAs = 0.01*np.array(get_EQE_AlGaAs(x, theta, (T_device + 273.15)))
            my_EQE_Si = 0.01*np.array(get_EQE_Si(x, theta, (T_device + 273.15)))
                
            EQE_GaAsAl_T_final = np.interp(wavelength_mykrom, wavelength_EQE_list*0.001, my_EQE_AlGaAs, left=0, right=0)
            EQE_Si_T_final = np.interp(wavelength_mykrom, wavelength_EQE_list*0.001, my_EQE_Si, left=0, right=0)
      
            
            # get EQE for AlGaAs and Si for diffuse irradiance
            if EQE_diffuse == True:
                
                counter_list_diffuse_AlGaAs = []
                denominator_list_diffuse_AlGaAs = []
                
                counter_list_diffuse_Si = []
                denominator_list_diffuse_Si = []                   
                
                
                theta_list_diffuse = np.array([0.0, 5, 10, 15, 20, 25, 30, 35, 40, 45,  50, 55, 60,  65, 70, 75, 80, 83.36])
                
                for t in theta_list_diffuse:
                    EQE_counter_diffuse_AlGaAs = np.array(get_EQE_AlGaAs(x, t, T_device + 273.15))*np.cos(np.radians(t))*np.sin(np.radians(t))
                    counter_list_diffuse_AlGaAs.append(EQE_counter_diffuse_AlGaAs)
                    EQE_denominator_diffuse_AlGaAs = np.cos(np.radians(t))*np.sin(np.radians(t))
                    denominator_list_diffuse_AlGaAs.append(EQE_denominator_diffuse_AlGaAs)
                    
                    EQE_counter_diffuse_Si = np.array(get_EQE_Si(x, t, T_device + 273.15))*np.cos(np.radians(t))*np.sin(np.radians(t))
                    counter_list_diffuse_Si.append(EQE_counter_diffuse_Si)
                    EQE_denominator_diffuse_Si = np.cos(np.radians(t))*np.sin(np.radians(t))
                    denominator_list_diffuse_Si.append(EQE_denominator_diffuse_Si)                        
                    
                    
                EQE_diffuse_AlGaAs = 0.01* (sum(counter_list_diffuse_AlGaAs) / sum(denominator_list_diffuse_AlGaAs))
                EQE_diffuse_Si = 0.01* (sum(counter_list_diffuse_Si) / sum(denominator_list_diffuse_Si))                    
                
                EQE_GaAsAl_T_final_diffuse = np.interp(wavelength_mykrom, wavelength_EQE_list*0.001, EQE_diffuse_AlGaAs, left=0, right=0)
                EQE_Si_T_final_diffuse = np.interp(wavelength_mykrom, wavelength_EQE_list*0.001, EQE_diffuse_Si, left=0, right=0)
                
                               
        
        if  T_device < -45.15:
            print('The device temperature is out of the range of this model')        
        if  T_device > 79.85:
            print('The device temperature is out of the range of this model')                                     
                     
                  
        EQE_T = []
        EQE_T.append(wavelength_mykrom)
        EQE_T.append(EQE_GaAsAl_T_final)
        EQE_T.append(EQE_Si_T_final)
                
                
        EQE_T_list.append(EQE_T)
        
        
        if EQE_diffuse == True:
            
            EQE_T_diffuse = []

            EQE_T_diffuse.append(wavelength_mykrom)
            EQE_T_diffuse.append(EQE_GaAsAl_T_final_diffuse)
            EQE_T_diffuse.append(EQE_Si_T_final_diffuse)
            EQE_T_list_diffuse.append(EQE_T_diffuse)                    
        
        
        if EQE_diffuse == True:
            
            if diffuse_spectra[0] == False:
                
                       
                Factor_direct = DNI_list[i]*np.cos(np.radians(theta)) / spectrum_irradiance_list[i]
                if Factor_direct > 1:
                    Factor_direct = 1
                
                if math.isnan(Factor_direct) == True:
                    Factor_direct = 1 #no sunshine...-> no current
                                                                               
                Factor_direct_list.append(Factor_direct)
                

                J_ph_AlGaAs_diffuse = np.trapz(np.array((1-Factor_direct)*spectrum_flux_mA_cm2)*np.array(EQE_GaAsAl_T_final_diffuse), wavelength_mykrom)
                J_ph_Si_diffuse = np.trapz(np.array((1-Factor_direct)*spectrum_flux_mA_cm2)*np.array(EQE_Si_T_final_diffuse), wavelength_mykrom) 


                J_ph_AlGaAs = np.trapz(np.array(Factor_direct*spectrum_flux_mA_cm2)*np.array(EQE_GaAsAl_T_final), wavelength_mykrom)
                J_ph_Si = np.trapz(np.array(Factor_direct*spectrum_flux_mA_cm2)*np.array(EQE_Si_T_final), wavelength_mykrom)


            if diffuse_spectra[0] == True:

                
                J_ph_AlGaAs_diffuse = np.trapz(np.array(spectrum_flux_mA_cm2_diffuse)*np.array(EQE_GaAsAl_T_final_diffuse), wavelength_mykrom)
                J_ph_Si_diffuse = np.trapz(np.array(spectrum_flux_mA_cm2_diffuse)*np.array(EQE_Si_T_final_diffuse), wavelength_mykrom) 
                
                J_ph_AlGaAs = np.trapz(np.array(spectrum_flux_mA_cm2)*np.array(EQE_GaAsAl_T_final), wavelength_mykrom)
                J_ph_Si = np.trapz(np.array(spectrum_flux_mA_cm2)*np.array(EQE_Si_T_final), wavelength_mykrom)
        
        
        
        
        if EQE_diffuse == False:

                    
            J_ph_AlGaAs = np.trapz(np.array(spectrum_flux_mA_cm2)*np.array(EQE_GaAsAl_T_final), wavelength_mykrom)
            J_ph_Si = np.trapz(np.array(spectrum_flux_mA_cm2)*np.array(EQE_Si_T_final), wavelength_mykrom)
        
                                             
        
        
        if EQE_diffuse == False:
        
            jsh_top = J_ph_AlGaAs/1000  #A/cm²
            jsh_bottom = J_ph_Si/1000 #A/cm²
            
            
        
        if EQE_diffuse == True:
        
            jsh_top = J_ph_AlGaAs/1000 + J_ph_AlGaAs_diffuse/1000        #A/cm²
            jsh_bottom = J_ph_Si/1000 +  J_ph_Si_diffuse/1000         #A/cm²
        
                    
            J_ph_AlGaAs_direct_list.append(J_ph_AlGaAs/1000) 
            J_ph_AlGaAs_diffuse_list.append(J_ph_AlGaAs_diffuse/1000) 
            J_ph_Si_direct_list.append(J_ph_Si/1000) 
            J_ph_Si_diffuse_list.append(J_ph_Si_diffuse/1000) 
        
        
        
        j_sh_bottom_list.append(jsh_bottom)
        j_sh_top_list.append(jsh_top)
        
    
        current_mismatch_abs = jsh_top-jsh_bottom
        current_mismatch_abs_list.append(current_mismatch_abs)
        current_mismatch_rel = 100*(jsh_top - jsh_bottom) / (jsh_top + jsh_bottom) 
        current_mismatch_rel_list.append(current_mismatch_rel)
        
        

        A01_Si =  wanlass_paramter_list[0]
        B01_Si= wanlass_paramter_list[1]
        beta02_Si =  wanlass_paramter_list[2]
        I01_T_Si= A01_Si*((np.exp(B01_Si*Eg_bottom_device))*(((T_device + 273.15))**3)*(np.exp(-Eg_bottom_device/(phys_const.k/phys_const.e*(T_device + 273.15)))))
        I02_T_Si= beta02_Si*((((T_device + 273.15))**(2.5))*np.exp(-Eg_bottom_device/(2*phys_const.k/phys_const.e*(T_device + 273.15))))
        
                   
        A01_AlGaAs = wanlass_paramter_list[3]
        B01_AlGaAs= wanlass_paramter_list[4]
        beta02_AlGaAs = wanlass_paramter_list[5]
        I01_T_AlGaAs= A01_AlGaAs*((np.exp(B01_AlGaAs*Eg_top_device))*(((T_device + 273.15))**3)*(np.exp(-Eg_top_device/(phys_const.k/phys_const.e*(T_device + 273.15)))))
        I02_T_AlGaAs= beta02_AlGaAs*((((T_device + 273.15))**(2.5))*np.exp(-Eg_top_device/(2*phys_const.k/phys_const.e*(T_device + 273.15))))
        
                               
        
        VT = (phys_const.k/phys_const.e)*(T_device + 273.15)
        
                          
        def CurrentFunc(I, V,Is1,Is2,Rs,Rp,Isc):
                Vp   = V - (Rs * I)                   #V
                Isc  = -Isc
                Iph  = Isc                            #A/cm²
                Iph += (Is1 * (np.exp(Rs * Isc / (1* VT)) - 1.0))
                Iph += (Is2 * (np.exp(Rs * Isc / (2 * VT)) - 1.0))
                Iph += ((Rs / Rp) * Isc)
                fI   = Iph
                fI  += (Is1 * (np.exp(Vp / (1 * VT)) - 1.0))
                fI  += (Is2 * (np.exp(Vp / (2 * VT)) - 1.0))
                fI  += (Vp / Rp)
                fI  -= I
                return fI                  
                          
          
        def calculateCurrent(V,Is1,Is2,Rs,Rp,Isc):
                fIZ  = Isc
                fIZ += (Is1 * (np.exp(V / (1 * VT)) - 1.0))
                fIZ += (Is2 * (np.exp(V / (2 * VT)) - 1.0))
                fIZ += (V / Rp)
                fI   = spo.fsolve(CurrentFunc, x0=fIZ, args=(V,Is1,Is2,Rs,Rp,Isc))
                return (fI)                


        U = np.arange(0, 2, 0.001)
        
        
        diode_current_Si = []
        
        Rs_Si = Rs_Rp_list[0]
        Rp_Si = Rs_Rp_list[1]
        
        
        for V in U:
            my_Si_current = calculateCurrent(V,I01_T_Si, I02_T_Si,Rs_Si,Rp_Si, jsh_bottom)
            diode_current_Si.append(my_Si_current[0])
        
        
        diode_current_AlGaAs = []
        
        Rs_AlGaAs = Rs_Rp_list[2]
        Rp_AlGaAs = Rs_Rp_list[3]
        
        
        for V in U:
            my_AlGaAs_current = calculateCurrent(V,I01_T_AlGaAs, I02_T_AlGaAs,Rs_AlGaAs,Rp_AlGaAs, jsh_top)
            diode_current_AlGaAs.append(my_AlGaAs_current[0])

        diode_current_AlGaAs_np = np.array(diode_current_AlGaAs)*(-1)
        diode_current_Si_np = np.array(diode_current_Si)*(-1)

        jsh_mj = min(diode_current_AlGaAs_np[0], diode_current_Si_np[0])


        
        j_mj = np.arange(0, jsh_mj, 0.00001)
        U_op_Si = []
        
        

        

        for k in range(len(j_mj)):
            my_index = find_nearest(diode_current_Si_np, j_mj[k])
            U_op_Si.append(U[my_index])


        U_op_AlGaAs = []

        for l in range(len(j_mj)):
            my_index = find_nearest(diode_current_AlGaAs_np, j_mj[l])
            U_op_AlGaAs.append(U[my_index])


        U_mj = np.array(U_op_AlGaAs) + np.array(U_op_Si)


        U_mj_0 = np.append(U_mj, [0])
        I_mj_0 = np.append(j_mj, [jsh_mj])


        U = np.arange(0, 2.5, 0.0001) 

        I_mj_0_int = np.interp(U, U_mj_0[::-1] , I_mj_0[::-1])
    
        diode_current = I_mj_0_int*1000 
        
     
        IV_solar_cell = []
        
        IV_solar_cell.append(U)
        IV_solar_cell.append(diode_current)
        
    
        IV_solar_cells.append(IV_solar_cell) #V, mA/cm²

    
        Power_max = max(diode_current*10 * U)           #W/m²
        PV_eff = 100 * Power_max/(spectrum_irradiance_list[i])      #%
        PV_eff_list.append(PV_eff)
        
        #index of V_oc
        idx = find_nearest(np.array(diode_current), 0)
        V_oc =  U[idx]
        j_sh = diode_current[0]
        FF = 100* Power_max / ( V_oc*j_sh*10) #%
        
        FF_list.append(FF)
        j_sh_list.append(j_sh)
        V_oc_list.append(V_oc)



        #get IV 2-electrode water splitting
               
        tafel_slope = IV_cat_parameters[0]
        distance = IV_cat_parameters[1]
        K0_mem = IV_cat_parameters[2]
        Ea_mem = IV_cat_parameters[3]
        j0 = IV_cat_parameters[4]
        Acti_E = IV_cat_parameters[5]
        
        
        if thermally_coupled == True:
        
            cat_current_T = cat_solar_area_ratio * (tafel_slope/(distance / ((K0_mem * np.exp(- Ea_mem / (phys_const.R*((T_device+273.15))))))))*np.real(lambertw((j0*np.exp((Acti_E/phys_const.R)*((1/298.15)-(1/(T_device+273.15)))))*(distance / ((K0_mem * np.exp(- Ea_mem / (phys_const.R*((T_device+273.15)))))))*np.exp((U-(1.4746 - 0.0008212*(T_device+273.15)))/tafel_slope)/tafel_slope))
            
        
        if thermally_coupled == False:
            
            cat_current_T = cat_solar_area_ratio * (tafel_slope/(distance / ((K0_mem * np.exp(- Ea_mem / (phys_const.R*((T_out_list[i]+273.15))))))))*np.real(lambertw((j0*np.exp((Acti_E/phys_const.R)*((1/298.15)-(1/(T_out_list[i]+273.15)))))*(distance / ((K0_mem * np.exp(- Ea_mem / (phys_const.R*((T_out_list[i]+273.15)))))))*np.exp((U-(1.4746 - 0.0008212*(T_out_list[i]+273.15)))/tafel_slope)/tafel_slope))


        

      
        IV_catalysts = [U, cat_current_T*1000]
        
        IV_catalysts_list.append(IV_catalysts) #V, mA/cm²
        
               
             
        #get operation current density
        curr_element = np.where(abs(diode_current - cat_current_T*1000) == min(abs(diode_current - cat_current_T*1000)))[0][0]
        j_op = diode_current[curr_element]     
        j_op_list.append(j_op)   #mA/cm2


        #Coupling_efficiency
        U_op = U[curr_element]   
        Couple_eff = 100 * j_op*10*U_op / Power_max #%
           
        Couple_eff_list.append(Couple_eff)

        E_op_list.append(U_op )

        #get STH efficiency
        
        STH_eff = j_op*1.23 / (spectrum_irradiance_list[i]/1000)
        
        STH_eff_list.append(STH_eff) 
        
        
        #get kg/H2 per hour       

        molar_mass_H2 = 2*1.00794/1000 #kg/mol 
        kg_H2_per_hour_m2 = 10*j_op*molar_mass_H2*3600 / (2*phys_const.e*phys_const.N_A) 
        #10-> mA/cm² in A/m², 3600 -> s/h
        kg_H2_per_hour_m2_list.append(kg_H2_per_hour_m2)
        
    #get kg/H2 per year
    kg_H2_per_year_m2 = sum(kg_H2_per_hour_m2_list)
        
                     
            
    Result_Dict = collections.OrderedDict()
    
    
    if reduced_output == False:  
        
    
        Result_Dict['Hour'] = Hour_list
        Result_Dict['T_outdoor'] = T_out_list
        Result_Dict['T_device'] = T_device_list
        Result_Dict['Irradiance'] = spectrum_irradiance_list
        Result_Dict['IV_solar_cell'] = IV_solar_cells
        Result_Dict['IV_catalysts'] = IV_catalysts_list
        Result_Dict['j_op'] = j_op_list
        Result_Dict['E_op'] = E_op_list
        Result_Dict['STH_eff'] = STH_eff_list
        Result_Dict['kg_H2_h_m²'] = kg_H2_per_hour_m2_list
        Result_Dict['kg_H2_year_m²'] = kg_H2_per_year_m2
        Result_Dict['j_mismtach_abs'] = np.array(current_mismatch_abs_list)
        Result_Dict['j_mismtach_rel'] = np.array(current_mismatch_rel_list)
        Result_Dict['Photon_flux_integrated'] = Total_integrated_photon_flux_list #mA/cm²
        Result_Dict['j_sh_bottom'] = j_sh_bottom_list
        Result_Dict['j_sh_top'] = j_sh_top_list
        Result_Dict['j_sh'] = j_sh_list
        Result_Dict['V_oc'] = V_oc_list
        Result_Dict['EQE_T'] = EQE_T_list
        Result_Dict['PV_eff'] = PV_eff_list
        Result_Dict['FF'] = FF_list
        Result_Dict['Couple_eff'] = Couple_eff_list
        Result_Dict['Eg_top'] = Eg_top_list
        Result_Dict['Eg_bottom'] = Eg_bottom_list
        Result_Dict['Solar_zenith'] = Solar_Zenith_Angle_list 
        Result_Dict['Solar_azimuth'] = Solar_Azimuth_Angle_list
        Result_Dict['Panel_tilt'] = Panel_tilt_list 
        Result_Dict['Panel_azimuth'] = Panel_azimuth_list
        Result_Dict['theta'] = theta_list
        Result_Dict['DNI'] = DNI_list
        Result_Dict['DHI'] = DHI_list
        Result_Dict['GHI'] = GHI_list
        Result_Dict['APE'] = APE_list
        
        
        if EQE_diffuse == True:
            Result_Dict['Factor_direct'] = Factor_direct_list
            Result_Dict['EQE_T_diffuse'] = EQE_T_list_diffuse
            Result_Dict['j_sh_top_direct'] = J_ph_AlGaAs_direct_list 
            Result_Dict['j_sh_top_diffuse']= J_ph_AlGaAs_diffuse_list
            Result_Dict['j_sh_bottom_direct']= J_ph_Si_direct_list
            Result_Dict['j_sh_bottom_diffuse']= J_ph_Si_diffuse_list
            
            
    if reduced_output == True:       
        Result_Dict['Hour'] = Hour_list
        Result_Dict['kg_H2_h_m²'] = kg_H2_per_hour_m2_list
        Result_Dict['kg_H2_year_m²'] = kg_H2_per_year_m2
            

    return Result_Dict


    
        




def t_fit_routine_electrolyzer(T_data_list, T_list,
                               guess_list = [0.035, 0.01, 0.05, 1e-4, 7.09071e-7, 30000 ], 
                               delta_T = 0.01, plot_data = True):
    
    '''This function fits experimental temperature dependent IV curves 
        of electrolyzers.

                                  

     Parameters
     ----------
     T_data_list: List of .dat files
         dat files containing T-dependent IV curves
        (2 columns: Voltage [V], Current density [A/cm²], no headers)
                 

     T_list: list of floats
         Corresponding temperatures of the T_data_list

    guess_list: list of floats
         Guess list for the Fit parameters: tafel_slope [V/dec], 
         electrode_distance [cm], ref_conductivity_membrane [S/cm] , 
         activation_energy_membrane [J/Mol], j0 [A/cm²] , activation_energy [J/Mol].
         Good starting point: [0.035, 0.01, 0.05, 1e-4, 7.09071e-7, 40000]
        

     delta_T: float
         Defines how much the temperature can be varied during the fitting 
         routine[K]
         
    plot_data: boolean
        If true, the input data with the fits are plotted.
         
         
    Returns the resulting parameters in ordered dictionairy
     containing the keys:


        * tafel_slope - Effective tafel slope in [V/dec]

        * electrode_distance - Electrode distance in [cm]      

        * ref_conductivity_membrane - Reference membrane conductivity in [S/cm]
        
        * activation_energy_membrane - Activation energy memrbane in [J/Mol]        

        * j0 - Effective exchange current density in [A/cm²]
        
        * activation_energy - Effective activitaion energy in [J/Mol]
        
        * T_fitted_list - List with fitted operation temperatures [K] 

     
    '''
    
    
    # Interpolate to highest T
    Voltage = np.array(T_data_list[-1][:,1])

    
    dct_IV_cats = {}
    
    for i in np.arange(len(T_data_list)):
        dct_IV_cats['IV_cat_T%s' % i] = np.array(np.interp(T_data_list[-1][:,1], T_data_list[i][:,1], T_data_list[i][:,0]))/1000 #A/cm²
    
      
    x, y = Voltage, dct_IV_cats['IV_cat_T0']

    def fit_func(x, tafel_slope, distance, K0_mem, Ea_mem, j0, Acti_E, T_fitted):
        return (tafel_slope/(distance / ((K0_mem * np.exp(- Ea_mem / (phys_const.R*(T_fitted)))))))*np.real(lambertw((j0*np.exp((Acti_E/phys_const.R)*((1/298.15)-(1/T_fitted))))*(distance / ((K0_mem * np.exp(- Ea_mem / (phys_const.R*(T_fitted))))))*np.exp((x-(1.4746 - 0.0008212*T_fitted))/tafel_slope)/tafel_slope))
        
    
    my_new_y = []
    
    for i in np.arange(len(T_data_list)):
        my_new_y.append(dct_IV_cats['IV_cat_T'+str(i)])
        
    my_new_y_np = np.array(my_new_y)
    

    def concatenate(x, tafel_slope, distance,K0_mem, Ea_mem, j0, Acti_E,*args):
        
        Input_list = []
        for a in args:
            Input_list.append(fit_func(x, tafel_slope, distance, K0_mem, Ea_mem, j0, Acti_E, a))
            
        return_tuple = tuple(Input_list )              
        return np.concatenate(return_tuple)
    
   
    
    guess_list = guess_list + T_list
    
    bounds_list_min = [0, 0, 0, 0, 1e-12, 0 ]
    
    for i in np.arange(len(T_data_list)):
        bounds_list_min.append(T_list[i]-delta_T)
    
    bounds_list_max = [np.inf, np.inf, np.inf, np.inf, 1e-5, np.inf]
    
    for i in np.arange(len(T_data_list)):
        bounds_list_max.append(T_list[i]+delta_T)
       
    
    (tafel_slope, distance, K0_mem, Ea_mem, j0, Acti_E, *T_fitted), _ = curve_fit(concatenate, x, my_new_y_np.ravel(), 
            p0 = guess_list,  
            bounds=(bounds_list_min, bounds_list_max))
      
    
    
    
    IV_fit_list = []
    
    
    U = np.arange(1.2, 2.2, 0.00025)
    
    cmap = plt.cm.get_cmap('coolwarm')
    
    
     # Plot the raw and experimental data
    if plot_data == True:
    
        fig,axes = plt.subplots( ncols=1, nrows=1, figsize=(5, 5),   gridspec_kw={'width_ratios':[1], 'height_ratios':[1] })
           
        
        for i in np.arange(len(T_data_list)):
            
            axes.scatter(Voltage, dct_IV_cats['IV_cat_T%s' % i], marker = "x", s = 25, label = 'Exp. T = ' + str(T_list[i]) + ' K', color = cmap(i / len(T_data_list) ))
        
        axes.set_xlabel("Voltage [V]", fontsize=18) 
        axes.set_ylabel("Current density [A/cm²]", fontsize=18)
        axes.tick_params(axis='x', labelsize = 15)
        axes.tick_params(axis='y', labelsize = 15)
        axes.set_xlim(1.23,1.9)
        axes.set_ylim(0, 0.15)
      
        
    for i in np.arange(len(T_data_list)):
        IV_cat_fit = (tafel_slope/(distance / ((K0_mem * np.exp(- Ea_mem / (phys_const.R*(T_fitted[i])))))))*np.real(lambertw((j0*np.exp((Acti_E/phys_const.R)*((1/298.15)-(1/T_fitted[i]))))*(distance / ((K0_mem * np.exp(- Ea_mem / (phys_const.R*(T_fitted[i]))))))*np.exp((U-(1.4746 - 0.0008212*T_fitted[i]))/tafel_slope)/tafel_slope))
        IV = []
        IV.append(U)
        IV.append(IV_cat_fit)
        IV_fit_list.append(IV)
        
        if plot_data == True:
            axes.plot(U, IV_cat_fit ,  color = cmap(i / len(T_data_list)), linewidth = 0.9)            
            axes.legend(fontsize=14)


    
    Result_Dict = collections.OrderedDict()
    Result_Dict['tafel_slope'] = tafel_slope
    Result_Dict['electrode_distance'] = distance
    Result_Dict['ref_conductivity_membrane'] = K0_mem
    Result_Dict['activation_energy_membrane'] = Ea_mem
    Result_Dict['j0'] = j0
    Result_Dict['activation_energy'] = Acti_E
    Result_Dict['T_fitted_list'] = [T_fitted]
    Result_Dict['IV_fits'] = IV_fit_list
    
           
    return (Result_Dict)
    
    
    
    
    
    
    
    
def perimeter_to_surface_ratio(pore_diameter, pitch, cell_length = 0.1,
                            cell_width = 0.1, passivation_eff = 0):
    
    
    """This function calculates the perimeter-to-surface ratio of porous 
    photoelectrodes to account for edge recombination losses.
     
    Parameters
    ----------
    pore_diameter: float
        Diameter of the pores in [m] 
        
    pitch: float
        Pitch of the pores in [m]    
     
    cell_length: float
        Length of the cell in [m]
        
    cell_width: float
        Width of the cell in [m]
    
    passivation_eff: float
        Edge passivation efficiency (between 0 and 1). Reduces the 
        perimeter-to-surface ratio to account for potential edge passivation.
        
            
    Returns
    -------
     Returns an ordered dictionairy containing the following keys:
         * perimeter_to_surface_ratio - Float: perimeter-to-surface ratio 
         to account for edge recombination losses in [cm] per [cm2]
         * remaining_area - Flot: Remaining portion of absorber active 
         surface area (between 0 and 1).      
    """
    
    #Cell surface area in m2
    surface = cell_length*cell_width #m2
        
    #Edge length in m2   
    edge_length = 2*cell_length + 2*cell_width
    
    #Number of pores 
    if pitch == 0:
        number_pores_total = 0
    if pitch == 0:  
        number_pores_total = 0
    else:
        number_pores_length = cell_length/pitch
        number_pores_width = cell_width/pitch
        number_pores_total =  number_pores_length*number_pores_width
    
    #Perimeter of one pore in m2
    perimeter_one_pore = pore_diameter*math.pi
    
    #Total perimeter
    perimeter_total = edge_length + number_pores_total*perimeter_one_pore
    
    #Perimeter [cm] per [cm2]
    perimeter_total_per_cm2 = (perimeter_total*100) / (surface*10000)
    
    perimeter_total_per_cm2_pass_eff = perimeter_total_per_cm2*(1-passivation_eff)  # [cm] per [cm2]
        
    area_total_pores = number_pores_total*math.pi*(0.5*pore_diameter)**2
       
    remaining_area =  (surface - area_total_pores) / surface
    
    Result_Dict = collections.OrderedDict()
    Result_Dict['perimeter_to_surface_ratio'] = perimeter_total_per_cm2_pass_eff
    Result_Dict['remaining_area'] = remaining_area

    return Result_Dict