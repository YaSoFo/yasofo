# README for YaSoFo - Yet Another SOlar Fuels Optimizer #

* YaSoFo implements some basic Python routines to calculate maximum theoretical efficiencies of single- and monolithic multi-junction solar cells for solar fuel generation. The starting point is a solar spectrum (typically AM1.5G), which can be manipulated by the application of a filter or an absorbing layer. The simple model employed here is described in the publication mentioned below.
* Project hosted at [https://bitbucket.org/YaSoFo/yasofo](https://bitbucket.org/YaSoFo/yasofo), mirrored at [https://codeberg.org/photon/YaSoFo](https://codeberg.org/photon/YaSoFo).
* Please cite [May et al., Sustainable Energy & Fuels, 1, 492-503 (2017). DOI:10.1039/C6SE00083E](http://pubs.rsc.org/en/content/articlelanding/2017/se/c6se00083e).
* Also solar-to-carbon efficiency calculations for photoelectrochemical negative emission approaches are implemented. For details, as well as an extended example, see [May and Rehfeld, Earth System Dynamics, 10, 1-7 (2019). DOI:10.5194/esd-10-1-2019](https://www.earth-syst-dynam.net/10/1/2019/).

## Dependencies ##

* Python 3.7 (or later) with SciPy (1.1 or later), for installation instructions see [SciPy](http://scipy.org/install.html).

## How to run tests? ##

We assume that yasofo.py and am15g.dat are in the same directory and you have installed SciPy >= 1.1. Open a Python terminal, e.g. Ipython, in the directory where yasofo.py is located. Now type the following in your terminal:

    import yasofo as yo
    yo.demo_single_junction()

for a demo for a single junction with coarse parameters.Type

    yo.demo_double_junction()

for a demo for a double junction with coarse parameters. The line

    yo.demo_triple_junction()

will give you a demo for a triple junction with coarse parameters, and finally

    yo.demo_quadruple_junction()

will calculate a quadruple junction with coarse parameters. The latter can take a long time. After some time - depending on the order of the multi-junction - a graph with the max. STF efficiency over \Delta G should appear.

Another - though not yet as powerful - option is to simply type in you terminal

    python3 yasofo.py

This will show the help. You can then run a demo with

    python3 yasofo.py -d
	

### Where to obtain further solar spectra? ###

The standard file format used here is equivalent to the format distributed with etaOpt by Fraunhofer ISE.
It can be downloaded from [their website](https://www.ise.fraunhofer.de/en/business-areas/photovoltaics/iii-v-and-concentrator-photovoltaics/iii-v-epitaxy-and-solar-cells/theoretical-modelling/etaopt.html). See also [Letay and Bett, Eur. Photovoltaic Sol. Energy Conf., Proc. Int. Conf., 17th, 2001, 178-181](https://www.researchgate.net/profile/Andreas_Bett/publication/259469995_EtaOpt_-_a_program_for_calculating_limiting_efficiency_and_optimum_bandgap_structure_for_multi-bandgap_solar_cells_and_TPV_cells/links/00b4952bedef00b41f000000.pdf).

In the etaOpt archive file, you will find a folder 'spectra', containing AM0, AM1.5D, and AM1.5G spectra.

Another source for spectra is e.g. [NREL](http://rredc.nrel.gov/solar/spectra/am1.5). There, one can download the high-resolution ASTM G-173-03 spectrum in the convenient .csv data format, to be found in the provided .zip archive. But beware! The computation time scales approximately with n^x, where n are the datapoints in your spectrum and x is the order of the multi-junction. You might want to use the 'change_spec_resolution' for changing the resolution to a convenient value. And check that the integrated is the one you want, i.e. 1000 W/m^2 for an AM1.5G spectrum, otherwise use the 'scaling' parameter of the 'solspec' class when importing a spectrum (would be 'scaling=1000' for the NREL-spectrum mentioned above).

The AM1.5G spectrum supplied with YaSoFo originates from etaOpt and is redistributed with friendly permission of the authors.

### Water absorption ###

A nice overview of the optical properties of water with data and references can be found at the [Oregon Medical Laser Center](http://omlc.org/spectra/water).

### Some quick examples ###

a) Let us assume you want to find the best double junction bandgap parameters for water splitting (E0=1.23eV).
Your absorbers are not the very best, so your open-circuit photovoltage is 500 mV lower than the bandgap/e, but you have very good catalysts, i.e. IrO2. Your solar spectrum is the unmodified AM 1.5G spectrum.

What would be the very best bandgap combination and what efficiency could you get out of it?

Start a python console, e.g. Ipython3 in the directory where you have yasofo.py and your solar spectrum.

    import yasofo as yo
    yo.find_max_double(E0=1.23, voltage_loss=0.5, only_print_results=True, db=False)

You should see a text stating max. efficiency and bandgap combination.

b) If you want to see the various bandgap combinations with their resulting performance:

    yo.best_gaps_double(E0=1.23, voltage_loss=0.5, db=False)

You should now see a 2d plot with the STF efficienciy as a function of the bandgaps.

If your absorber is approaching the detailed balance limit, you can calculate the detailed balance limit by setting db=False (or omitting it completely), the voltage_loss variable is then ignored, if present:

    yo.best_gaps_double(E0=1.23)

c) You want to modify you AM1.5G spectrum by the absorbance of a catalyst or electrolyte. Let us assume
you have a file 'absorbance.dat' with your wavelength (nm) in the first column and you absorbance (1/cm)
in the second column, has 5 lines of header and your absorbing layer is 5 mm thick.

    am15g_mod=yo.mod_spectrum_by_abs('absorbance.dat', thickness=0.5, header=5)

You can plot the spectrum to see if things are fine

    am15g_mod.plot()

Let us now see how a tandem performs with the modified spectrum:

    yo.best_gaps_double(data=am15g_mod, E0=1.23, voltage_loss=0.5, db=False)

## Contact ##

* Matthias May. Email: Matthias.May ]at[ uni-tuebingen.de

## License ##

Licensed under the [EUPL, Version 1.2](https://joinup.ec.europa.eu/software/page/eupl).
