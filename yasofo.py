# -*- coding: utf-8 -*-
"""# YaSoFo - Yet Another SOlar Fuels Optimizer #

Version 1.5.3 (09. Dec 2024)

Authors: Matthias May (2016-2024), Moritz Koelbach (2020-2022)

License: EUPL, Version 1.2.
See https://joinup.ec.europa.eu/software/page/eupl

Updates and more info can be found under https://bitbucket.org/YaSoFo/yasofo .
Mirrored at https://codeberg.org/photon/YaSoFo .

Tested for Python 3.12 with SciPy 1.11 (www.scipy.org) on Linux.
Further reference spectra can be downloaded e.g. from Fraunhofer ISE or NREL,
see README.md.

Assumptions:

* Monolithic stack, i.e. current matching.
* Absorption is unity above the bandgap, 0 below.
* Constant reduction of open-circuit voltage per absorber due to recombination
    (semi-empirical model) or radiative recombination (detailed balance).
* One-diode equation is matched with catalyst characteristic to obtain the
    operating current.
* Occasional snapshots are archived on Zenodo: DOI:10.5281/zenodo.1489157
* For more details of the model, see publication below.

Please cite May et al., Sustainable Energy & Fuels (2017).
    DOI:10.1039/C6SE00083E
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as phys_const
#import scipy.special.lambertw as lambertw
from scipy.special import lambertw
import argparse
import sys
from itertools import repeat
from multiprocessing import Pool
from multiprocessing import cpu_count
from copy import deepcopy
from src.spectrum import solspec, change_spec_resolution, am15g, mod_spectrum
from src.spectrum import mod_spectrum_by_abs, nm_to_ev, ev_to_nm
from src.solarctica import calc_device_t, match_catalysts_solar_cell_current, get_STH_eff
from src.water_layer_function import find_max_double_water_layer
# from numba import autojit

# general functions

version = '1.5.3'
print('This is YaSoFo version', version)

# [exchange current density anode [A*cm^-2], Tafel slope anode [V*dec^-1],
# ohmic resistivity [Ohm], ex. c. dens cathode, Tafel sl. cathode]:
# default anode: IrO2
cat_odrop = [7.09071e-07, 0.036486402, 0., 0, 0]
# factor to get from power (W/cm^2) to efficiency (0 to 1) assuming 1000 W/m^2.
etafactor = 10
# refraction indices over and under the junction [n_o, n_u]. [1, 0] assumes
# air above, a perfect mirror on the back
ref_ind = [1, 0]
# electrons per CO2 molecule for STC calculation
ec = 2
# names and formats  for the standard structured arrays of the output:
datanames = ['E0', 'gaps', 'j', 'P', 'best_comb']
dataformats = (float, float, float, float)
# set up multiprocessing:
# (This might break things under MS Windows, If so, revert to version 1.1.5.)
numproc = cpu_count()

if type(am15g) is not None:
    max_gap = nm_to_ev(min(am15g.data[:, 0]))
    min_gap = nm_to_ev(max(am15g.data[:, 0]))
else:
    max_gap = min_gap = 0


def match_cats_diode_current(deltaG, j_ph, U_oc, cat_para=cat_odrop, nid=1.0,
                             temp=300):
    """Matches the diode current to CV (+ohmic drop) of a catalyst.
       Default: Exp. det. IrO2 with no ohmic drop for anode.
       Returns current where catalyst CV and diode current intersect.

       Parameters
       ----------
       deltaG: float
           Target DeltaG.
       j_ph: float
           Saturation photocurrent in A cm^-2.
       U_oc: float
           Open-circuit voltage in V.
       cat_para: list
           Catalyst parameters: [exchange current density anode, Tafel slope
           anode, ohmic resistivity, exchange current density cathode, Tafel
           slope cathode]. Default: Exp. IrO2 with no ohmic resistivity as
           anode.
       nid: float
           Diode ideality factor. Default: 1.0
       temp: int
           Temperature. Default: 300K.
       """
    if deltaG > U_oc:
        current = 0.0
    else:
        # stepsize for voltage range 2.5mV <- this impacts results for low j
        U = np.arange(deltaG, U_oc, 0.0025)
        U_T = phys_const.k*temp/phys_const.e
        diode_current = -j_ph*(1-(np.exp(U/(nid*U_T))-1)/((np.exp(U_oc/(nid*U_T))-1)))
        if abs(cat_para[2]) < 1e-6 and cat_para[3] == 0:
            cat_current = -cat_para[0]*np.exp((U-deltaG)/cat_para[1])
        elif abs(cat_para[2]) < 1e-6 and cat_para[3] > 0:  # no ohmic drop
        #TODO: check speed of if-else switch vs. general formula
            cat_current = -(np.exp((U-deltaG + cat_para[1]*np.log(cat_para[0])+
                cat_para[3]*np.log(cat_para[4]))/(cat_para[1] + cat_para[3])))
        else:
            if cat_para[3] == 0:
            #TODO: implement for calc with ohmic drop
                cat_current = -(cat_para[1]/cat_para[2])*np.real(lambertw(cat_para[0]*
                    cat_para[2]*np.exp((U-deltaG)/cat_para[1])/cat_para[1]))
            else:
                print('not yet implemented!')
                return 0
    # find the voltage, where the difference between catalyst current and
    # one-diode current is smallest
        curr_element = np.where(abs(diode_current - cat_current) == min(abs(diode_current
            - cat_current)))[0][0]
        current = -diode_current[curr_element]
    return current


# @autojit
def mpp_single_diode_current(j_ph, U_oc, nid=1.0, temp=300):
    """Finds the maximum power point by employing a single diode current.
    Returns resulting power, U_mpp, j_mpp.

    Parameters
    ----------
    U_oc: float
        Open-circuit voltage
    j_ph: float
        Photocurrent.
    nid: float
        Diode ideality factor.
    temp: float
        Temperature in K.
    """
    U = np.arange(U_oc - 2.0, U_oc + 0.01, 0.001)
    U_T = phys_const.k*temp/phys_const.e
    # use one-diode equation, calculate power, and take max as MPP
    diode_current = -j_ph*(1-(np.exp(U/(nid*U_T))-1)/((np.exp(U_oc/(nid*U_T))-1)))
    power = -diode_current*U
    max_power = max(power)
    if U_oc <= 0.0:
        max_power = 0.0
        U_mpp = 0.0
        j_mpp = 0.0
    else:
        U_mpp = U[np.nonzero(power == max(power))[0][0]]
        j_mpp = diode_current[np.nonzero(power == max(power))[0][0]]
    return max_power, U_mpp, j_mpp


def show_output(power, best_comb, spectrum=am15g, etascale=False, stc=False,
                C=1.):
    """Produces a simple plot of the results. If etascale is False, the
    efficiency definition assumes 1000 W/m^2.

    Parameters
    ----------
    power: list
        Output from calculation, actual data.
    best_comb: list
        Output from calculation, best combination.
    C: float
        Light concentration.
    """
    etaf_local = etafactor/C
    if C != 1.:
        print('Light concentration of %6.2f used.' % C)
    if etascale is True and stc is False:
        etaf_local = 10000/(spectrum.power*C)
        print('Efficiencies scaled to total power of spectrum, %5.2f W/m^2.'
              % (spectrum.power*C))
    if stc is True:
        print('Highest STC efficiency:\n E0:', best_comb['E0'], '\n Bandgaps (eV):',
              best_comb['gaps'], '\n STC (%): ', best_comb['P'])
        plt.plot(power['E0'], power['P'])
    else:
        print('Highest STF efficiency:\n E0:', best_comb['E0'], '\n Bandgaps (eV):',
              best_comb['gaps'], '\n STF (%): ', best_comb['P']*etaf_local*100)
        plt.plot(power['E0'], power['P']*100*etaf_local)
    plt.xlabel('$E_0$ / eV')
    plt.ylabel('Max. efficiency / %')
    plt.show()


# @autojit
def Voc_det_balance(gap, spectrum=am15g, upper_gap=max_gap, nid=1.0,
                    temperature=300, photo_current=np.NaN, r_ind=ref_ind,
                    C=1.):
    """Calculates open-circuit voltage in the det. balance limit.
       Returns: V_oc. We're using n_o=1, n_u=0, see eq. 5 in Letay & Bett,
       Eur. Photovoltaic Sol. Energy Conf., Proc. Int. Conf., 17th (2001), but
       without the sign error in eq. 8.

    Parameters
    ----------
    gap: float
        Bandgap of the absorber in question.
    spectrum: solspec
        The spectrum.
    upper_gap: float
        Bandgap of the cell above (if present).
    nid: float
        Diode ideality factor.
    temperature: float
        Solar cell temperature in K.
    r_ind: list
        Refractive indices of medium over and under the junction [n_o, n_u]
    C: float
        Light concentration factor. Default: 1.0.
        Caution: In most routines, concentration is already incorporated in the
        increased photo_current.
    """
    if photo_current <= 0:
        return 0.
    kT = phys_const.k*temperature
    ri_factor = r_ind[0]**2 + r_ind[1]**2
    gapSI = gap*phys_const.e
    # Boltzmann approximation (factor 10k from m2 to cm2):
    j_0 = (2*np.pi*phys_const.e*ri_factor/(phys_const.h**3*phys_const.c**2)*kT
           *(gapSI**2 + 2*kT*gapSI + 2*(kT)**2))*np.exp(-gapSI/kT)/10000
    j_0n = j_0**(1/nid)
    if np.isnan(photo_current):
        photo_current = spectrum.integrate_flux(upper_gap, gap)*C
    V_oc = nid*kT/phys_const.e*np.log(photo_current/j_0n - 1)
    # np isnan(V_oc) _is_ True does somehow not work here:
    if np.isnan(V_oc) == True:
        V_oc = 0
    #   print('V_oc NaN, setting to 0!', flush=True)
    return V_oc

# single junction functions
# NB: Alas, quite a bit of the following is basically a violation of the DRY
# principle, but reusable functions reduce the computational performance...

def power_single_E0(spectrum, E0, voltage_loss=0.4, cat_para=cat_odrop,
                    nid=1.0, db=True, stc=False, C=1.):
    """Calculates the power for a single junction with given E0, overpotential
       and voltage loss.
       Returns: Best combination. [band_gap, current, current*E0].

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    E0: float
        DeltaG.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    cat_para: list
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic drop, exc. c.d. cathode, T.s. cathode]. Default: Exp. IrO2
        with no ohmic resistivity.
    nid: float
        Diode ideality factor.
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    sc: list
        If sc[0] is True, the solar-to-carbon efficiency is calculated with
        sc[1] electrons per CO2 molecule and returned in per cent.
    C: float
        Light concentration factor. Default: 1.0
    Returns
    -------
    single_junction: list
        List comprised of bandgap, current density (mA/cm2), electrochemical
        power (current*E0). The latter has to be scaled with the etafactor to
        get the efficiency.
    """
    current = 0
    single_junction = [0, 0, 0]
    max_gap = nm_to_ev(min(spectrum.data[:, 0]))
    min_gap = nm_to_ev(max(spectrum.data[:, 0]))
    # probe bandgaps in the range from E0 to E0 + 2 eV
    if E0 <= min_gap:
        bandgap_range = np.arange(min_gap, E0 + 2, 0.01)
    else:
        bandgap_range = np.arange(E0, E0 + 2, 0.01)
    for band_gap in bandgap_range:
        photo_current = spectrum.integrate_flux(max_gap, band_gap)*C
        if db is False:
            if band_gap < 2*voltage_loss:
                U_oc = band_gap/2
            else:
                U_oc = band_gap - voltage_loss
        else:
            U_oc = Voc_det_balance(band_gap, spectrum, max_gap, nid=nid,
                                   photo_current=photo_current)
        current = match_cats_diode_current(E0, photo_current, U_oc, cat_para, nid)
        if current > single_junction[1]:
            single_junction[1] = current
            single_junction[0] = band_gap
    single_junction[2] = single_junction[1]*E0
    if stc is True:
        single_junction[2] = current_to_stc(spectrum, single_junction[1], ec,
                       C)
    return single_junction


def max_power_single_E0(spectrum=am15g, min_E0=0.1, max_E0=2.0, stepsize=0.05,
                        voltage_loss=0.4, cat_para=cat_odrop, nid=1.0, C=1.,
                        plotting=True, db=True, etascale=False, stc=False):
    """Calculates maximum theoretical efficiencies for single junctions as a
    function of E0. Returns dict of arrays [E0, gaps, j, P, best_comb].
    Power P in  (W/cm^2).

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    min_E0: float
        Minimum E0 to be considered.
    max_E0: float
        Maximum E0 to be considered.
    stepsize: float
        Steysize in V by which E0 is scanned.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic resistivity, ecd cathode, Ts cathode]. Default: Exp. IrO2
        with no ohmic resistivity.
    nid: float
        Diode ideality factor.
    plotting: bool
        Plot results?
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    etascale: bool
        Shall the efficiency be determined via the total power of the spectrum
        (True), or the AM1.5G standard, i.e. 1000 W (False)?
    """
    E0_range = np.arange(min_E0, max_E0, stepsize)
    pwr = []
    for E0 in E0_range:
        print('E0:', E0, flush=True)
        temp_power = power_single_E0(spectrum, E0=E0, voltage_loss=voltage_loss,
            cat_para=cat_para, nid=nid, db=db, stc=stc, C=C)
        pwr.append([E0, temp_power[0], temp_power[1], temp_power[2]])
    power = np.array(pwr)
    result = dict(zip(datanames[:-1], power.T))
    result[datanames[-1]] = power[result['P'].argmax()]
    result[datanames[-1]].dtype = {'names':datanames[:-1],
                                   'formats':dataformats}
    if plotting is True:
        show_output(result, result[datanames[-1]], spectrum, etascale, stc, C)
    return result


def calc_single_junction(spectrum=am15g, power_E0=True, Eg=2.26, E0=1.23,
                         voltage_loss=0.4, cat_para=cat_odrop, nid=1.0, C=1.,
                         db=True, etascale=False, stc=False):
    """Calculates max. STF efficiency for a single junction with given bandgap
    and E0. Returns structured array [E0, gaps, j, P]. Power in
    (W/cm^2), current in(A/cm^2).

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    power_E0: bool
        Is the power determined by current*E0? Otherwise: j*V_MPP.
    Eg: float
        Bandgap (eV).
    E0: float
        E0 to be considered.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic drop, ecd cathode, Ts cathode]. Default: Exp. IrO2 with no
        ohmic resistivity.
    nid: float
        Diode ideality factor.
    etascale: bool
        Shall the efficiency be determined via the total power of the spectrum
        (True), or the AM1.5G standard, i.e. 1000 W (False)?
    """
    etaf_local = etafactor/C
    if etascale is True:
        etaf_local = 10000/spectrum.power
        print('Efficiencies scaled to total power of spectrum, %5.2f W/m^2.'
              % (spectrum.power*C))
    max_gap = nm_to_ev(min(spectrum.data[:, 0]))
    photo_current = spectrum.integrate_flux(max_gap, Eg)*C
    if db is False:
        if Eg < 2*voltage_loss:
            U_oc = Eg/2
        else:
            U_oc = Eg - voltage_loss
    else:
        U_oc = Voc_det_balance(Eg, spectrum, max_gap, nid=nid,
                               photo_current=photo_current)
    if power_E0 is True:
        current = match_cats_diode_current(E0, photo_current, U_oc, cat_para, nid)
        if stc is False:
            power = current*E0
            print('STF efficiency (%) with bandgap Eg=', Eg, 'eV for E0=', E0,
                  'eV:', power*100*etaf_local, ';\nCurrent (A/cm^2): ',
                  current)
        else:
            power = current_to_stc(spectrum, current, ec, C)
            print('STC efficiency (%) with bandgap Eg=', Eg, 'eV for E0=', E0,
                  'eV:', power, ';\nCurrent (A/cm^2): ', current)
    else:
        [power, U_mpp, current] = mpp_single_diode_current(photo_current,
            U_oc, nid, temp=300)
        print('Photovoltaic efficiency (%):', power*100*etaf_local,
              ';\nCurrent (A/cm^2): ', current, '.\n')
    result = np.zeros((1), dtype=[('E0', float), ('gaps', float,
                      (1, 1)), ('j', float), ('P', float)])
    result['P'] = power
    result['j'] = current
    result['E0'] = E0
    result['gaps'] = [Eg]
    return result

# double junction functions


def find_max_double(spectrum=am15g, power_E0=True, E0=1.23, voltage_loss=0.4,
                    allow_thinning=True, cat_para=cat_odrop, nid=1.0,
                    only_print_results=False, db=True, etascale=False,
                    stc=False, C=1.):
    """Find the maximum current for a double junction at a given E0.
    Returns [Eg_hi, Eg_lo, j] and [Eg_hi, Eg_lo, j, P]. P in W/cm^2.

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    power_E0: bool
        Is the power determined by current*E0? Otherwise: j*V_MPP.
    min_E0: float
        Minimum E0 to be considered.
    max_E0: float
        Maximum E0 to be considered.
    stepsize: float
        Steysize in V by which E0 is scanned.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    allow_thinning: bool
        Shall we allow the top absorber to be thinned to transmit light to
        subsequent cell and thereby minimise current mismatch?
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic drop, ecd cathode, Ts cathode]. Default: Exp. IrO2 with no
        ohmic resistivity.
    nid: float
        Diode ideality factor.
    only_print_results: bool
        Only print the results in a formatted way, do not return values.
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    etascale: bool
        Shall the efficiency be determined via the total power of the spectrum
        (True), or the AM1.5G standard, i.e. 1000 W (False)?
    """
    etaf_local = etafactor/C
    if etascale is True:
        etaf_local = 10000/(spectrum.power*C)
        print('Efficiencies scaled to total power of spectrum, %5.2f W/m^2.'
              % (spectrum.power*C))
    current = 0.0
    photo_current = [0.0, 0.0]
    power = 0.0
    max_gap = nm_to_ev(min(spectrum.data[:, 0]))
    # we store the overall best combination regarding the current/power here
    best_combination_current = [0.0, 0.0, 0.0]
    best_combination_power = [0.0, 0.0, 0.0, 0.0]
    # go through all the wavelengths for the upper cell
    for lower_lambda in spectrum.data[1:-2, 0]:
        i = np.where(spectrum.data[:, 0] > lower_lambda)[0][0]
        # bottom cell cannot have higher bandgap than top cell
        for higher_lambda in spectrum.data[i:, 0]:
            higher_gap = nm_to_ev(lower_lambda)
            lower_gap = nm_to_ev(higher_lambda)

            # bottom cell
            photo_current[1] = spectrum.integrate_flux(higher_lambda,
                lower_lambda, electronvolts=False)*C
            # top cell
            photo_current[0] = spectrum.integrate_flux(lower_lambda,
                         spectrum.data[0, 0], electronvolts=False)*C
            # current matching condition
            min_photo_current = min([photo_current[0], photo_current[1]])
            if (allow_thinning is True) and (photo_current[0] > photo_current[1]):
                # make top cell thinner to transmit light to bottom cell
                average_current = np.average(photo_current)
                min_photo_current = average_current
                photo_current[0] = photo_current[1] = average_current
            if db is False:
                if lower_gap < 2*voltage_loss:
                    U_oc = higher_gap + lower_gap/2 - voltage_loss
                else:
                    U_oc = higher_gap + lower_gap - 2*voltage_loss
            else:
                U1 = Voc_det_balance(higher_gap, spectrum, max_gap,
                                     nid=nid, photo_current=photo_current[0])
                U2 = Voc_det_balance(lower_gap, spectrum, higher_gap,
                                     nid=nid, photo_current=photo_current[1])
                U_oc = U1 + U2
            if power_E0 is False:
                [power, U_mpp, current] = mpp_single_diode_current(min_photo_current,
                    U_oc, nid, temp=300)
            else:
                current = match_cats_diode_current(E0, min_photo_current, U_oc,
                                                   cat_para, nid)
                if current > best_combination_current[2]:
                    best_combination_current = [higher_gap, lower_gap, current]
                if U_oc < E0:
                    power = 0.0
                else:
                    power = current*E0
            if power > (best_combination_power[3]):
                best_combination_power = [higher_gap, lower_gap, current, power]
    if stc is True:
        best_combination_power[-1] = current_to_stc(spectrum,
                              best_combination_power[-2], ec, C)
    if only_print_results is True:
        if power_E0 is True and stc is False:
            print('Highest STF efficiency at E0=', E0, 'eV :',
                  best_combination_power[-1]*100*etaf_local,
                  '%.\n Bandgaps (eV): ', best_combination_power[0:2])
        elif stc is True:
            print('Highest STC efficiency at E0=', E0, 'eV :',
                  best_combination_power[-1], '%.\n Bandgaps (eV): ',
                  best_combination_power[0:2])
            print('(Under the assumption of %i electrons per product.)' % ec)
        else:
            print('Highest photovoltaic efficiency of :', best_combination_power[-1]*100*etaf_local,
                  '%.\n Bandgaps (eV): ', best_combination_power[0:2])
        return None
    return best_combination_current, best_combination_power


def max_power_double_E0(spectrum=am15g, min_E0=1.0, max_E0=3.0, stepsize=0.05,
                        voltage_loss=0.4, allow_thinning=True, cat_para=cat_odrop,
                        nid=1.0, C=1., plotting=True, db=True, etascale=False,
                        stc=False):
    """Calculates max. powers for double junctions as a function of E0. Returns
       a dictionary with keys [E0, gaps, j, P, best_comb].

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    min_E0: float
        Minimum E0 to be considered.
    max_E0: float
        Maximum E0 to be considered.
    stepsize: float
        Steysize in V by which E0 is scanned.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    allow_thinning: bool
        Shall we allow the top absorber to be thinned to transmit light to
        subsequent cell and thereby minimise current mismatch?
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic resistivity, ecd cathode, Ts cathode]. Default: Exp. IrO2
        with no ohmic resistivity.
    nid: float
        Diode ideality factor.
    plotting: bool
        Plot results at the end?
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    etascale: bool
        Shall the efficiency be determined via the total power of the spectrum
        (True), or the AM1.5G standard, i.e. 1000 W (False)?
    """
    pool = Pool(processes=numproc)
    E0_range = np.arange(min_E0, max_E0, stepsize)
    power = np.stack(pool.starmap(mpd_loop, zip(E0_range, repeat(spectrum),
                repeat(voltage_loss), repeat(allow_thinning),
                repeat(cat_para), repeat(nid), repeat(db),
                repeat(etascale), repeat(stc), repeat(C))))
    pool.close()
    best_comb =  power[power['P'].argmax()]
    tlist = []
    for dname in power.dtype.names:
        tlist.append(power[dname])
    result = dict(zip(power.dtype.names, tlist))
    result['best_comb'] = best_comb
    print('Finished!')
    if plotting is True:
        show_output(result, result[datanames[-1]], spectrum, etascale, stc, C)
    return result


def mpd_loop(E0, spectrum, voltage_loss, allow_thinning, cat_para, nid,
             db, etascale, stc, C):
    """Calculates output of a double junction for a single E0. Returns
       a structured array [E0, gaps, current, power].

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    allow_thinning: bool
        Shall we allow the top absorber to be thinned to transmit light to
        subsequent cell and thereby minimise current mismatch?
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic drop, ecd cathode, Ts cathode]. Default: Exp. IrO2 with no
        ohmic resistivity.
    nid: float
        Diode ideality factor.
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    """
    print(E0, flush=True)
    res = np.zeros((1), dtype=[('E0', float), ('gaps', float, (1, 2)),
                               ('j', float), ('P', float)])
    res['E0'] = E0
    tmp = find_max_double(spectrum, True, E0, voltage_loss, allow_thinning,
                          cat_para, nid, False, db, etascale, stc, C)
    res['gaps'] = tmp[0][0:2]
    res['j'] = tmp[0][2]
    res['P'] = tmp[1][-1]
    return res


def best_gaps_double(spectrum=am15g, power_E0=True, E0=1.23, voltage_loss=0.4,
                     allow_thinning=True, cat_para=cat_odrop, nid=1.0,
                     plotting=True, db=True, etascale=False, stc=False, C=1.):
    """Calculate 2d plot of efficiency vs. gaps for a double junction. Returns
    dictionary with keys [gap_axis, efficiency].

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    power_E0: bool
        Is the power determined by current*E0? Otherwise: j*V_MPP.
    min_E0: float
        Minimum E0 to be considered.
    max_E0: float
        Maximum E0 to be considered.
    stepsize: float
        Stepsize in V by which E0 is scanned.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    allow_thinning: bool
        Shall we allow the top absorber to be thinned to transmit light to
        subsequent cell and thereby minimise current mismatch?
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic resistivity, ecd cathode, Ts cathode]. Default: Exp. IrO2
        with no ohmic resistivity.
    nid: float
        Diode ideality factor.
    plotting: bool
        Plot results at the end?
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    etascale: bool
        Shall the efficiency be determined via the total power of the spectrum
        (True), or the AM1.5G standard, i.e. 1000 W (False)?
    """
    current = 0.0
    etaf_local = etafactor/C
    max_gap = nm_to_ev(min(spectrum.data[:, 0]))
    photo_current = [0.0, 0.0]  # for [higher_gap,lower_gap]
    efficiency = np.zeros([spectrum.length, spectrum.length])
    if etascale is True:
        etaf_local = 10000/(spectrum.power/C)
        print('Efficiencies scaled to total power of spectrum, %5.2f W/m^2.'
              % (spectrum.power*C))
    gap_axis = nm_to_ev(spectrum.data[:, 0])
    for lower_lambda in spectrum.data[1:-2, 0]:
        i = np.where(spectrum.data[:, 0] > lower_lambda)[0][0]
        for higher_lambda in spectrum.data[i:, 0]:
            j = np.where(spectrum.data[:, 0] == higher_lambda)[0][0]
            higher_gap = nm_to_ev(lower_lambda)
            lower_gap = nm_to_ev(higher_lambda)
            if lower_gap < 2*voltage_loss:
                U_oc = higher_gap + lower_gap/2 - voltage_loss
            else:
                U_oc = higher_gap + lower_gap - 2*voltage_loss
            photo_current[1] = spectrum.integrate_flux(higher_lambda, lower_lambda,
                electronvolts=False)*C
            photo_current[0] = spectrum.integrate_flux(lower_lambda, spectrum.data[0, 0],
                electronvolts=False)*C
            min_photo_current = min([photo_current[0], photo_current[1]])
            if (allow_thinning is True) and (photo_current[0] > photo_current[1]):
                average_current = np.average(photo_current)
                min_photo_current = average_current
                photo_current[0] = photo_current[1] = average_current
            if db is False:
                if lower_gap < 2*voltage_loss:
                    U_oc = higher_gap + lower_gap/2 - voltage_loss
                else:
                    U_oc = higher_gap + lower_gap - 2*voltage_loss
            else:
                U1 = Voc_det_balance(higher_gap, spectrum, max_gap, nid=nid,
                                     photo_current=photo_current[0])
                U2 = Voc_det_balance(lower_gap, spectrum, higher_gap,
                                     nid=nid, photo_current=photo_current[1])
                U_oc = U1 + U2
            if power_E0 is True:
                current = match_cats_diode_current(E0, min_photo_current, U_oc,
                                                   cat_para, nid)
                if U_oc < E0:
                    efficiency[i, j] = 0
                else:
                    efficiency[i, j] = current*E0
            else:
                mpp = mpp_single_diode_current(min_photo_current, U_oc,
                                               nid, temp=300)
                efficiency[i, j] = mpp[0]
    efficiency = efficiency*etaf_local
    if stc is True:
        efficiency = current_to_stc(spectrum, efficiency/(E0*etaf_local), ec,
                                    C)/100
    if plotting is True:
        fig, axes = plt.subplots(nrows=1, ncols=1)
        p1 = axes.pcolor(gap_axis, gap_axis, efficiency*100, cmap=plt.cm.jet,
                         vmin=abs(efficiency*100).min(), vmax=abs(efficiency*100).max())
        axes.set_xlabel("Bottom cell bandgap / eV", fontweight='bold', fontsize=14)
        axes.set_ylabel("Top cell bandgap / eV", fontweight='bold', fontsize=14)
        axes.tick_params(color='red')
        fig.subplots_adjust(right=0.85)
        cbar_ax = fig.add_axes([0.9, 0.15, 0.03, 0.7])
        cb = fig.colorbar(p1, cax=cbar_ax)
        if power_E0 is True:
            if stc is False:
                cb.set_label('STF / %')
            else:
                cb.set_label('STC / %')
        else:
            cb.set_label('Photovolt. eff. / %')
        plt.show()
    result = dict({'gap_axis': gap_axis, 'efficiency': efficiency})
    return result


def calc_double_junction(spectrum=am15g, power_E0=True, Eg1=1.85, Eg2=1.12,
                         E0=1.23, voltage_loss=0.4, allow_thinning=True,
                         cat_para=cat_odrop, nid=1.0, C=1., db=True,
                         etascale=False, stc=False):
    """Calculates max. STF efficiency for a double junction with two given
    bandgaps and E0. Returns structured array [E0, gaps, j, P]. Power in
    (W/cm^2), current in (A/cm^2).

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    power_E0: bool
        Is the power determined by current*E0? Otherwise: j*V_MPP.
    Eg1: float
        Top cell bandgap (eV).
    Eg2: float
        Bottom cell bandgap (eV).
    E0: float
        E0 to be considered.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    allow_thinning: bool
        Shall we allow the top absorber to be thinned to transmit light to
        subsequent cell and thereby minimise current mismatch?
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic drop, ecd cathode, Ts cathode]. Default: Exp. IrO2 with no
        ohmic resistivity.
    nid: float
        Diode ideality factor.
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    etascale: bool
        Shall the efficiency be determined via the total power of the spectrum
        (True), or the AM1.5G standard, i.e. 1000 W (False)?
    """
    etaf_local = etafactor/C
    if etascale is True:
        etaf_local = 10000/(spectrum.power/C)
        print('Efficiencies scaled to total power of spectrum, %5.2f W/m^2.'
              % (spectrum.power*C))
    photo_current = [0.0, 0.0]
    max_gap = nm_to_ev(min(spectrum.data[:, 0]))
    photo_current[0] = spectrum.integrate_flux(99, Eg1)*C
    photo_current[1] = spectrum.integrate_flux(Eg1, Eg2)*C
    min_photo_current = min([photo_current[0], photo_current[1]])
    if (allow_thinning is True) and (photo_current[0] > photo_current[1]):
        average_current = 0.5*(photo_current[0] + photo_current[1])
        min_photo_current = average_current
        photo_current[0] = photo_current[1] = average_current
    if db is False:
        if Eg2 < 2*voltage_loss:
            U_oc = Eg1 + Eg2/2 - voltage_loss
        else:
            U_oc = Eg1 + Eg2 - 2*voltage_loss
    else:
        U1 = Voc_det_balance(Eg1, spectrum, max_gap, nid=nid,
                             photo_current=photo_current[0])
        U2 = Voc_det_balance(Eg2, spectrum, Eg1,
                             nid=nid, photo_current=photo_current[1])
        U_oc = U1 + U2
    if power_E0 is True:
        current = match_cats_diode_current(E0, min_photo_current, U_oc,
                                           cat_para, nid)
        if stc is False:
            power = current*E0
            print('STF efficiency (%) with bandgaps Eg1, Eg2 =', Eg1, ',', Eg2,
                  'eV for E0=', E0, 'eV:', power*100*etaf_local,
                  ';\nCurrent (A/cm^2): ', current)
        else:
            power = current_to_stc(spectrum, current, ec, C)
            print('STC efficiency (%) with bandgaps Eg1, Eg2 =', Eg1, ',', Eg2,
                  'eV for E0=', E0, 'eV:', power, ';\nCurrent (A/cm^2): ',
                  current)
    else:
        [power, U_mpp, current] = mpp_single_diode_current(min_photo_current,
            U_oc, nid, temp=300)
        print('Photovoltaic efficiency (%) with bandgaps Eg1, Eg2 =', Eg1, ',',
              Eg2, 'eV:', power*100*etaf_local, ';\nCurrent (A/cm^2): ',
              current, '.\n')
    result = np.zeros((1), dtype=[('E0', float), ('gaps', float, (1, 2)),
                                  ('j', float), ('P', float)])
    result['P'] = power
    result['j'] = current
    result['E0'] = E0
    result['gaps'] = [Eg1, Eg2]
    return result

# triple junction functions


def max_power_triple_E0(spectrum=am15g, min_E0=1.0, max_E0=3.0, stepsize=0.05,
                        voltage_loss=0.4, allow_thinning=True, cat_para=cat_odrop,
                        nid=1.0, homotop=False, plotting=True, db=True,
                        etascale=False, stc=False, C=1.):
    """Calculates powers for three junctions. Returns dict with keys [E0, P,
       gaps, j, best_comb]. NB: Detailed balance (db=True) will increase
       walltime by ca. 200%.

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    min_E0: float
        Minimum E0 to be considered.
    max_E0: float
        Maximum E0 to be considered.
    stepsize: float
        Steysize in V by which E0 is scanned.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    allow_thinning: bool
        Shall we allow the top absorber to be thinned to transmit light to
        subsequent cell and thereby minimise current mismatch?
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic drop, ecd cathode, Ts cathode]. Default: Exp. IrO2 with no
        ohmic resistivity.
    nid: float
        Diode ideality factor.
    homotop: bool
        If true, the top two absorbers have the same bandgap, i.e. creating a
        double junction of the very same material.
    plotting: bool
        Plot results at the end?
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    stc: bool
        Calculate Solar-to-Carbon efficiency?
    etascale: bool
        Shall the efficiency be determined via the total power of the spectrum
        (True), or the AM1.5G standard, i.e. 1000 W (False)?
    """
    pool = Pool(processes=numproc)
    E0_range = np.arange(min_E0, max_E0, stepsize)
    datatypes = {'names':['E0', 'gaps', 'j', 'P'], 'formats':[float,
                 (float, (1, 3)), float, float]}
    best_comb = np.zeros((1), dtype=datatypes)
    power = np.stack(pool.starmap(mpt_loop, zip(E0_range, repeat(spectrum),
                repeat(voltage_loss), repeat(allow_thinning), repeat(cat_para),
                repeat(nid), repeat(db), repeat(homotop), repeat(stc),
                repeat(C))))
    pool.close()
    best_comb = power[power['P'].argmax()]
    tlist = []
    for dname in power.dtype.names:
        tlist.append(power[dname])
    result = dict(zip(power.dtype.names, tlist))
    result['best_comb'] = best_comb
    print('Finished!')
    if plotting is True:
        show_output(power, best_comb, spectrum, etascale, stc, C)
    return result


def mpt_loop(E0, spectrum, voltage_loss, allow_thinning, cat_para, nid, db,
             homotop, stc, C):
    """Calculates output of a triple junction for a single E0. Returns
       structured array [E0, [gaps], current, power].

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    allow_thinning: bool
        Shall we allow the top absorber to be thinned to transmit light to
        subsequent cell and thereby minimise current mismatch?
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic drop, ecd cathode, Ts cathode]. Default: Exp. IrO2 with no
        ohmic resistivity.
    nid: float
        Diode ideality factor.
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    homotop: bool
        If true, the top two absorbers have the same bandgap, i.e. creating a
        double junction of the very same material.
    """
    print('E0', E0, flush=True)
    res = np.zeros((1), dtype=[('E0', float), ('gaps', float, (1, 3)),
                               ('j', float), ('P', float)])
    res['E0'] = E0
    if homotop is True:
        tmp = find_max_triple_homotop(spectrum, True, E0, voltage_loss,
                                      allow_thinning, cat_para, nid, False, db,
                                      stc, C)
    else:
        tmp = find_max_triple(spectrum, True, E0, voltage_loss, allow_thinning,
                              cat_para, nid, False, db, stc, C)
    res['gaps'] = tmp[0][0:3]
    res['j'] = tmp[0][3]
    res['P'] = tmp[1][-1]
    return res


def find_max_triple(spectrum=am15g, power_E0=True, E0=1.23, voltage_loss=0.4,
                    allow_thinning=True, cat_para=cat_odrop, nid=1.0,
                    only_print_results=False, db=True, etascale=False,
                    stc=False, C=1.):
    """Find the maximum current for a triple junction. Returns [Eg_hi(eV),
    Eg_med, Eg_lo(eV),j(mAcm-2)] and [Eg_hi(eV), Eg_med,Eg_lo(eV),j(mAcm-2), P]
    allow_thinning: allow the upper cell to be thinned to transmit light to
    lower cell.

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    power_E0: bool
        Is the power determined by current*E0? Otherwise: j*V_MPP.
    min_E0: float
        Minimum E0 to be considered.
    max_E0: float
        Maximum E0 to be considered.
    stepsize: float
        Steysize in V by which E0 is scanned.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    allow_thinning: bool
        Shall we allow the top absorber to be thinned to transmit light to
        subsequent cell and thereby minimise current mismatch?
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic drop, ecd cathode, Ts cathode]. Default: Exp. IrO2 with no
        ohmic resistivity.
    nid: float
        Diode ideality factor.
    only_print_results: bool
        Only print the results in a formatted way, do not return values.
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    stc: bool
        Solar-to-Carbon Efficiency calculation?
    etascale: bool
        Shall the efficiency be determined via the total power of the spectrum
        (True), or the AM1.5G standard, i.e. 1000 W (False)?
    """
    etaf_local = etafactor/C
    if etascale is True:
        etaf_local = 10000/(spectrum.power*C)
        print('Efficiencies scaled to total power of spectrum, %5.2f W/m^2.'
              % (spectrum.power*C))
    current = 0.0
    max_gap = nm_to_ev(min(spectrum.data[:, 0]))
    photo_current = [0, 0, 0]
    power = 0.0
    best_combination_current = [0, 0, 0, 0]
    best_combination_power = [0, 0, 0, 0]
    for lower_lambda in spectrum.data[2:-2, 0]:
        i = np.where(spectrum.data[:, 0] > lower_lambda)[0][0]
        for medium_lambda in spectrum.data[i:-3, 0]:
            j = np.where(spectrum.data[:, 0] > medium_lambda)[0][0]
            for higher_lambda in spectrum.data[j:-4, 0]:
                higher_gap = nm_to_ev(lower_lambda)
                medium_gap = nm_to_ev(medium_lambda)
                lower_gap = nm_to_ev(higher_lambda)
                # it would be nicer to use calc_.. function here, but that
                # decreases speed by ca. 10% (at least with my interpreter)
                # to speed things up:
                if (power_E0 is True) and (0.87*(higher_gap + medium_gap +
                        lower_gap) < E0):
                    power = 0.0
                else:
                    photo_current[2] = spectrum.integrate_flux(higher_lambda,
                        medium_lambda, electronvolts=False)*C
                    photo_current[1] = spectrum.integrate_flux(medium_lambda,
                        lower_lambda, electronvolts=False)*C
                    photo_current[0] = spectrum.integrate_flux(lower_lambda,
                        spectrum.data[0, 0], electronvolts=False)*C
                    if (allow_thinning is True) and (photo_current[0] >
                            photo_current[1]) and (photo_current[0] > 0.0015)\
                            and (photo_current[1] > 0.0015):
                        average_current = np.average(photo_current[0:2])
                        if average_current > photo_current[2]:
                            average_current = np.average(photo_current)
                            photo_current[2] = average_current
                        photo_current[0] = average_current
                        photo_current[1] = average_current
                    elif (allow_thinning is True) and (photo_current[1] > photo_current[2]):
                        average_current = np.average(photo_current[1:])
                        if photo_current[0] > average_current:
                            average_current = np.average(photo_current)
                            photo_current[0] = average_current
                        photo_current[1] = average_current
                        photo_current[2] = average_current
                    min_photo_current = min([photo_current[0], photo_current[1],
                                             photo_current[2]])
                    if db is False:
                        if lower_gap < 2*voltage_loss:
                            U_oc = higher_gap + lower_gap/2 + medium_gap - 2*voltage_loss
                        else:
                            U_oc = higher_gap + lower_gap + medium_gap - 3*voltage_loss
                    else:
                        U1 = Voc_det_balance(higher_gap, spectrum, max_gap, nid=nid,
                                             photo_current=photo_current[0])
                        U2 = Voc_det_balance(medium_gap, spectrum, higher_gap,
                                nid=nid, photo_current=photo_current[1])
                        U3 = Voc_det_balance(lower_gap, spectrum, medium_gap,
                                nid=nid, photo_current=photo_current[2])
                        U_oc = U1 + U2 + U3
                    if power_E0 is False:
                        [power, U_mpp, current] = mpp_single_diode_current(min_photo_current,
                            U_oc, nid, temp=300)
                    else:
                        current = match_cats_diode_current(E0, min_photo_current,
                            U_oc, cat_para, nid)
                        power = current*E0
                    if current > best_combination_current[-1]:
                        best_combination_current = [higher_gap, medium_gap,
                            lower_gap, current]
                    if power > (best_combination_power[-1]):
                        best_combination_power = [higher_gap, medium_gap,
                            lower_gap, current, power]
    if stc is True:
        best_combination_power[-1] = current_to_stc(spectrum,
                              best_combination_power[-2], ec, C)
    if only_print_results is True:
        if power_E0 is True and stc is False:
            print('Highest STF efficiency at E0=', E0, 'eV :',
                  best_combination_power[-1]*100*etaf_local,
                  '%.\n Bandgaps (eV): ', best_combination_power[0:3])
        elif stc is True:
            print('Highest STC efficiency at E0=', E0, 'eV :',
                  best_combination_power[-1], '%.\n Bandgaps (eV): ',
                  best_combination_power[0:3])
            print('(Under the assumption of %i electrons per product.)' % ec)
        else:
            print('Highest photovoltaic efficiency of :', best_combination_power[-1]*100*etaf_local,
                  '%.\n Bandgaps (eV): ', best_combination_power[0:3])
        return None
    return best_combination_current, best_combination_power


def find_max_triple_homotop(spectrum=am15g, power_E0=True, E0=1.23, voltage_loss=0.4,
                            cat_para=cat_odrop, nid=1.,
                            only_print_results=False, db=True, etascale=False,
                            stc=False, C=1.):
    """Find the maximum current for a triple junction, but top two gaps are the
    same. Returns [Eg_hi, Eg_med, Eg_lo, j(mAcm-2)] and
    [Eg_hi, Eg_med, Eg_lo, j(mAcm-2), P].

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    power_E0: bool
        Is the power determined by current*E0? Otherwise: j*V_MPP.
    E0: float
        E0 to be considered.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic drop, ecd cathode, Ts cathode]. Default: Exp. IrO2 with no
        ohmic resistivity.
    nid: float
        Diode ideality factor.
    only_print_results: bool
        Only print the results in a formatted way, do not return values.
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    etascale: bool
        Shall the efficiency be determined via the total power of the spectrum
        (True), or the AM1.5G standard, i.e. 1000 W (False)?
    """
    etaf_local = etafactor/C
    if etascale is True:
        etaf_local = 10000/(spectrum.power*C)
        print('Efficiencies scaled to total power of spectrum, %5.2f W/m^2.'
              % (spectrum.power*C))
    current = 0.0
    max_gap = nm_to_ev(min(spectrum.data[:, 0]))
    photo_current = [0, 0]
    power = 0.0
    best_combination_current = [0, 0, 0]
    best_combination_power = [0, 0, 0, 0]
    for lower_lambda in spectrum.data[1:-2, 0]:
        i = np.where(spectrum.data[:, 0] > lower_lambda)[0][0]
        for higher_lambda in spectrum.data[i:, 0]:
        # the two top cells have the same bandgap
            higher_gap = nm_to_ev(lower_lambda)
            lower_gap = nm_to_ev(higher_lambda)
            photo_current[1] = spectrum.integrate_flux(higher_lambda,
                         lower_lambda, electronvolts=False)*C
            photo_current[0] = spectrum.integrate_flux(lower_lambda,
                         spectrum.data[0, 0], electronvolts=False)*C/2
            min_photo_current = min([photo_current[0], photo_current[1]])
            if db is False:
                if lower_gap < 2*voltage_loss:
                    U_oc = 2*higher_gap + lower_gap/2 - 2*voltage_loss
                else:
                    U_oc = 2*higher_gap + lower_gap - 3*voltage_loss
            else:
                U1 = Voc_det_balance(higher_gap, spectrum, max_gap, nid=nid,
                                     photo_current=photo_current[0])
                U2 = Voc_det_balance(lower_gap, spectrum, higher_gap,
                        nid=nid, photo_current=photo_current[1])
                U_oc = 2*U1 + U2
            if power_E0 is False:
                [power, U_mpp, current] = mpp_single_diode_current(min_photo_current,
                    U_oc, nid, temp=300)
            else:
                current = match_cats_diode_current(E0, min_photo_current, U_oc,
                                                   cat_para, nid)
                if U_oc < E0:
                    power = 0.0
                else:
                    power = current*E0
            if current > best_combination_current[2]:
                best_combination_current = [higher_gap, lower_gap, current]
            if power > (best_combination_power[3]):
                best_combination_power = [higher_gap, lower_gap, current, power]
    if stc is True:
        best_combination_power[-1] = current_to_stc(spectrum,
                              best_combination_power[-2], ec, C)
    if only_print_results is True:
        if power_E0 is True and stc is False:
            print('Highest STF efficiency at E0=', E0, 'eV :',
                  best_combination_power[-1]*100*etaf_local, '%.\n Bandgaps (eV): ',
                  [best_combination_power[0], best_combination_power[0],
                   best_combination_power[1]])
        elif stc is True:
            print('Highest STC efficiency at E0=', E0, 'eV :',
                  best_combination_power[-1], '%.\n Bandgaps (eV): ',
                  [best_combination_power[0], best_combination_power[0],
                   best_combination_power[1]])
            print('(Under the assumption of %i electrons per product.)' % ec)
        else:
            print('Highest photovoltaic efficiency of :',
                  best_combination_power[-1]*100*etaf_local, '%.\n Bandgaps (eV): ',
                  [best_combination_power[0], best_combination_power[0],
                   best_combination_power[1]])
        return None
    return best_combination_current, best_combination_power


def calc_triple_junction(spectrum=am15g, power_E0=True, Eg1=2.26, Eg2=1.12,
                         Eg3=0.67, E0=1.23, voltage_loss=0.4,
                         allow_thinning=True, cat_para=cat_odrop, nid=1.0,
                         db=True, etascale=False, stc=False, C=1.):
    """Calculates max. STF efficiency for a triple junction with three given
    bandgaps and E0. Returns structured array [E0, gaps, j, P]. Power in
    (W/cm^2), current in(A/cm^2).

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    power_E0: bool
        Is the power determined by current*E0? Otherwise: j*V_MPP.
    Eg1: float
        Top cell bandgap (eV).
    Eg2: float
        Middle cell bandgap (eV).
    Eg3: float
        Bottom cell bandgap (eV).
    E0: float
        E0 to be considered.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    allow_thinning: bool
        Shall we allow the top absorber to be thinned to transmit light to
        subsequent cell and thereby minimise current mismatch?
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic drop, ecd cathode, Ts cathode]. Default: Exp. IrO2 with no
        ohmic resistivity.
    nid: float
        Diode ideality factor.
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    stc: bool
        Calculate Solar-to-Carbon efficiency?
    etascale: bool
        Shall the efficiency be determined via the total power of the spectrum
        (True), or the AM1.5G standard, i.e. 1000 W (False)?
    """
    etaf_local = etafactor/C
    if etascale is True:
        etaf_local = 10000/(spectrum.power*C)
        print('Efficiencies scaled to total power of spectrum, %5.2f W/m^2.'
              % (spectrum.power*C))
    photo_current = [0.0, 0.0, 0.0]
    max_gap = nm_to_ev(min(spectrum.data[:, 0]))
    photo_current[0] = spectrum.integrate_flux(99, Eg1)*C
    photo_current[1] = spectrum.integrate_flux(Eg1, Eg2)*C
    photo_current[2] = spectrum.integrate_flux(Eg2, Eg3)*C
    if (allow_thinning is True) and (photo_current[0] >
            photo_current[1]) and (photo_current[0] > 0.0015)\
            and (photo_current[1] > 0.0015):
        average_current = np.average(photo_current[0:2])
        if average_current > photo_current[2]:
            average_current = np.average(photo_current)
            photo_current[2] = average_current
        photo_current[0] = average_current
        photo_current[1] = average_current
    elif (allow_thinning is True) and (photo_current[1] > photo_current[2]):
        average_current = np.average(photo_current[1:])
        if photo_current[0] > average_current:
            average_current = np.average(photo_current)
            photo_current[0] = average_current
        photo_current[1] = average_current
        photo_current[2] = average_current
    min_photo_current = min(photo_current)
    if db is False:
        if Eg3 < 2*voltage_loss:
            U_oc = Eg1 + Eg2 + Eg3/2 - 2*voltage_loss
        else:
            U_oc = Eg1 + Eg2 + Eg3 - 3*voltage_loss
    else:
        U1 = Voc_det_balance(Eg1, spectrum, max_gap, nid=nid,
                    photo_current=photo_current[0])
        U2 = Voc_det_balance(Eg2, spectrum, Eg1,
                nid=nid, photo_current=photo_current[1])
        U3 = Voc_det_balance(Eg3, spectrum, Eg2,
                nid=nid, photo_current=photo_current[2])
        U_oc = U1 + U2 + U3
    if power_E0 is True:
        current = match_cats_diode_current(E0, min_photo_current, U_oc,
                                           cat_para, nid)
        if stc is False:
            power = current*E0
            print('STF efficiency (%) with bandgaps Eg1, Eg2, Eg3 =', Eg1, ',',
                  Eg2, ',', Eg3, 'eV for E0=', E0, 'eV:', power*100*etaf_local,
                  ';\nCurrent (A/cm^2): ', current)
        else:
            power = current_to_stc(spectrum, current, ec, C)
            print('STC efficiency (%) with bandgaps Eg1, Eg2, Eg3 =', Eg1, ',',
                  Eg2, ',', Eg3, 'eV for E0=', E0, 'eV:', power,
                  ';\nCurrent (A/cm^2): ', current)
    else:
        [power, U_mpp, current] = mpp_single_diode_current(min_photo_current,
            U_oc, nid, temp=300)
        print('Photovoltaic efficiency (%) with bandgaps Eg1, Eg2, Eg3 =', Eg1,
        ',', Eg2, ',', Eg3, 'eV:', power*100*etaf_local, ';\nCurrent (A/cm^2): ', current, '.\n')
    result = np.zeros((1), dtype=[('E0', float), ('gaps', float,
                      (1, 3)), ('j', float), ('P', float)])
    result['P'] = power
    result['j'] = current
    result['E0'] = E0
    result['gaps'] = [Eg1, Eg2, Eg3]
    return result

# quadruple junction functions


def max_power_quadruple_E0(spectrum=am15g, min_E0=1.0, max_E0=3.0, stepsize=.2,
                           voltage_loss=0.4, allow_thinning=True,
                           cat_para=cat_odrop, nid=1.0,
                           tempfile='tmp_quad.dat', plotting=False, db=True,
                           etascale=False, C=1.):
    """Calculates powers for 4 junctions. Returns dict with keys [E0, gaps, j,
       P, best_comb].

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    power_E0: bool
        Is the power determined by current*E0? Otherwise: j*V_MPP.
    min_E0: float
        Minimum E0 to be considered.
    max_E0: float
        Maximum E0 to be considered.
    stepsize: float
        Steysize in V by which E0 is scanned.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    allow_thinning: bool
        Shall we allow the top absorber to be thinned to transmit light to
        subsequent cell and thereby minimise current mismatch?
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic drop, ecd cathode, Ts cathode]. Default: Exp. IrO2 with no
        ohmic resistivity.
    nid: float
        Diode ideality factor.
    tempfile: str
        File for storing temporary results during calculation. Not needed, but
        recommended due to long calc.
    plotting: bool
        Plot results at the end?
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    etascale: bool
        Shall the efficiency be determined via the total power of the spectrum
        (True), or the AM1.5G standard, i.e. 1000 W (False)?
    """
    print('NB: This is a very lengthy calculation and might take hours.')
    pool = Pool(processes=numproc)
    E0_range = np.arange(min_E0, max_E0, stepsize)
    datatypes = {'names':['E0', 'gaps', 'j', 'P'], 'formats':[float,
                 (float, (1, 4)),float,float]}
    best_comb = np.zeros((1), dtype=datatypes)
    power = np.stack(pool.starmap(mpq_loop, zip(E0_range, repeat(spectrum),
        repeat(voltage_loss), repeat(allow_thinning), repeat(cat_para),
        repeat(nid), repeat(db), repeat(tempfile), repeat(C))))
    pool.close()
    best_comb = power[power['P'].argmax()]
    tlist = []
    for dname in power.dtype.names:
        tlist.append(power[dname])
    result = dict(zip(power.dtype.names, tlist))
    result['best_comb'] = best_comb
    print('Finished!')
    if plotting is True:
        show_output(power, best_comb, spectrum, etascale, C)
    return result


def mpq_loop(E0, spectrum, voltage_loss, allow_thinning, cat_para, nid, db,
             tempfile, C):
    """Calculates output of a quadruple junction for a single E0. Returns
       structured array [E0, [gaps], current, power].

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    allow_thinning: bool
        Shall we allow the top absorber to be thinned to transmit light to
        subsequent cell and thereby minimise current mismatch?
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic drop, ecd cathode, Ts cathode]. Default: Exp. IrO2 with no
        ohmic resistivity.
    nid: float
        Diode ideality factor.
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    tempfile: str
        File for storing temporary results during calculation. Not needed, but
        recommended due to long calc.
    """
    print('E0: ', E0, flush=True)
    res = np.zeros((1), dtype=[('E0', float),('gaps',float,(1,4)),('j',float),('P',float)])
    res['E0'] = E0
    tmp = find_max_quadruple(spectrum, True, E0, voltage_loss, allow_thinning,
                             cat_para, nid, db, C)
    res['gaps'] = tmp[0][0:4]
    res['j'] = tmp[0][4]
    res['P'] = tmp[1][-1]
    if tempfile != '':
        saveres = np.concatenate([res['E0'], res['gaps'].flatten(), res['j'], res['P']])
        np.savetxt(tempfile, saveres, header='tempfile_quadruple_calc')
    return res


def find_max_quadruple(spectrum=am15g, power_E0=True, E0=1.23, voltage_loss=.4,
                       allow_thinning=True, cat_para=cat_odrop, nid=1.0,
                       db=True, C=1.):
    """Find the maximum current for a quadruple junction. Returns
    [Eg_highest, Eg_hi, Eg_med, Eg_lo,j(mAcm-2)] and
    [Eg_highest, Eg_hi, Eg_med, Eg_lo, j(mAcm-2), P].

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    power_E0: bool
        Is the power determined by current*E0? Otherwise: j*V_MPP.
    E0: float
        E0 to be considered.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    allow_thinning: bool
        Shall we allow the top absorber to be thinned to transmit light to
        subsequent cell and thereby minimise current mismatch?
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic drop, ecd cathode, Ts cathode]. Default: Exp. IrO2 with no
        ohmic resistivity.
    nid: float
        Diode ideality factor.
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    """
    current = 0.0
    max_gap = nm_to_ev(min(spectrum.data[:, 0]))
    photo_current = [0, 0, 0, 0]
    power = 0.0
    best_combination_current = [0, 0, 0, 0, 0]
    best_combination_power = [0, 0, 0, 0, 0]
    for lowest_lambda in spectrum.data[:-4, 0]:
        k = np.where(spectrum.data[:, 0] > lowest_lambda)[0][0]
        for lower_lambda in spectrum.data[k:-3, 0]:
            i = np.where(spectrum.data[:, 0] > lower_lambda)[0][0]
            for medium_lambda in spectrum.data[i:-2, 0]:
                j = np.where(spectrum.data[:, 0] > medium_lambda)[0][0]
                for higher_lambda in spectrum.data[j:, 0]:
                    highest_gap = nm_to_ev(lowest_lambda)
                    higher_gap = nm_to_ev(lower_lambda)
                    medium_gap = nm_to_ev(medium_lambda)
                    lower_gap = nm_to_ev(higher_lambda)
                    if lower_gap < 2*voltage_loss:
                        U_oc = highest_gap + higher_gap + medium_gap + lower_gap/2 - 3*voltage_loss
                    else:
                        U_oc = highest_gap + higher_gap + medium_gap + lower_gap - 4*voltage_loss
                    if (power_E0 is True) and (0.87*(highest_gap + higher_gap +
                            medium_gap + lower_gap) < E0):
                        power = 0.0
                    else:
                        photo_current[3] = spectrum.integrate_flux(higher_lambda,
                            medium_lambda, electronvolts=False)*C
                        photo_current[2] = spectrum.integrate_flux(medium_lambda,
                            lower_lambda, electronvolts=False)*C
                        photo_current[1] = spectrum.integrate_flux(lower_lambda,
                            lowest_lambda, electronvolts=False)*C
                        photo_current[0] = spectrum.integrate_flux(lowest_lambda,
                            spectrum.data[0, 0], electronvolts=False)*C
                        if (allow_thinning is True) and (photo_current[0] > photo_current[1]):
                            average_current = np.average(photo_current[:2])
                            if average_current > photo_current[2]:
                                average_current = np.average(photo_current[:3])
                                if average_current > photo_current[3]:
                                    average_current = np.average(photo_current)
                                    photo_current[3] = average_current
                                photo_current[2] = average_current
                            photo_current[0] = average_current
                            photo_current[1] = average_current
                        elif (allow_thinning is True) and (photo_current[1] > photo_current[2]):
                            average_current = np.average(photo_current[1:3])
                            if photo_current[0] > average_current:
                                average_current = np.average(photo_current[:3])
                                photo_current[0] = average_current
                            photo_current[1] = average_current
                            photo_current[2] = average_current
                        elif (allow_thinning is True) and (photo_current[2] > photo_current[3]):
                            average_current = np.average(photo_current[2:])
                            if photo_current[1] > average_current:
                                average_current = np.average(photo_current[1:])
                                photo_current[1] = average_current
                            photo_current[2] = average_current
                            photo_current[3] = average_current
                        min_photo_current = min([photo_current[0], photo_current[1],
                                        photo_current[2], photo_current[3]])
                        if db is False:
                            if lower_gap < 2*voltage_loss:
                                U_oc = (highest_gap + higher_gap + lower_gap/2 +
                                        medium_gap - 3*voltage_loss)
                            else:
                                U_oc = (highest_gap + higher_gap + lower_gap +
                                        medium_gap - 4*voltage_loss)
                        else:
                            U1 = Voc_det_balance(highest_gap, spectrum, max_gap, nid=nid,
                                        photo_current=photo_current[0])
                            U2 = Voc_det_balance(higher_gap, spectrum, highest_gap,
                                    nid=nid, photo_current=photo_current[1])
                            U3 = Voc_det_balance(medium_gap, spectrum, higher_gap,
                                    nid=nid, photo_current=photo_current[2])
                            U4 = Voc_det_balance(lower_gap, spectrum, medium_gap,
                                    nid=nid, photo_current=photo_current[3])
                            U_oc = U1 + U2 + U3 + U4
                        if power_E0 is False:
                            [power, U_mpp, current] = mpp_single_diode_current(min_photo_current,
                                U_oc, nid, temp=300)
                        else:
                            current = match_cats_diode_current(E0,
                                min_photo_current, U_oc, cat_para, nid)
                            power = current*E0
                        if current > best_combination_current[-1]:
                            best_combination_current = [highest_gap,
                                    higher_gap, medium_gap, lower_gap, current]
                        if power > (best_combination_power[-1]):
                            best_combination_power = [highest_gap, higher_gap,
                                        medium_gap, lower_gap, current, power]
    return best_combination_current, best_combination_power


def calc_quadruple_junction(spectrum=am15g, power_E0=True, Eg1=1.9, Eg2=1.4,
                            Eg3=1.0, Eg4=0.5, E0=1.23, voltage_loss=0.4,
                            allow_thinning=True, cat_para=cat_odrop, nid=1.0,
                            db=True, etascale=False, C=1.):
    """Calculates max. STF efficiency for a quadruple junction with four given
    bandgaps and E0. Returns structured array [E0, gaps, j, P]. Power in
    (W/cm^2), current in(A/cm^2).

    Parameters
    ----------
    spectrum: solspec
        The spectrum.
    power_E0: bool
        Is the power determined by current*E0? Otherwise: j*V_MPP.
    Eg1: float
        Top cell bandgap (eV).
    Eg2: float
        Second cell bandgap (eV).
    Eg3: float
        Third cell bandgap (eV).
    Eg4: float
        Bottom cell bandgap (eV).
    E0: float
        E0 to be considered.
    voltage_loss: float
        The photovoltage loss per junction compared to the bandgap in V.
    allow_thinning: bool
        Shall we allow the top absorber to be thinned to transmit light to
        subsequent cell and thereby minimise current mismatch?
    cat_para:
        Catalyst parameters: [exchange current density anode, Tafel slope
        anode, ohmic drop, ecd cathode, Ts cathode]. Default: Exp. IrO2 with no
        ohmic resistivity.
    nid: float
        Diode ideality factor.
    db: bool
        Calculate U_oc with det. balance, i.e. radiative recombination.
    etascale: bool
        Shall the efficiency be determined via the total power of the spectrum
        (True), or the AM1.5G standard, i.e. 1000 W (False)?
    """
    etaf_local = etafactor/C
    if etascale is True:
        etaf_local = 10000/(spectrum.power*C)
        print('Efficiencies scaled to total power of spectrum, %5.2f W/m^2.'
              % (spectrum.power*C))
    photo_current = [0.0, 0.0, 0.0, 0.0]
    max_gap = nm_to_ev(min(spectrum.data[:, 0]))
    photo_current[0] = spectrum.integrate_flux(99, Eg1)*C
    photo_current[1] = spectrum.integrate_flux(Eg1, Eg2)*C
    photo_current[2] = spectrum.integrate_flux(Eg2, Eg3)*C
    photo_current[3] = spectrum.integrate_flux(Eg3, Eg4)*C
    if (allow_thinning is True) and (photo_current[0] > photo_current[1]):
        average_current = np.average(photo_current[:2])
        if average_current > photo_current[2]:
            average_current = np.average(photo_current[:3])
            if average_current > photo_current[3]:
                average_current = np.average(photo_current)
                photo_current[3] = average_current
            photo_current[2] = average_current
        photo_current[0] = average_current
        photo_current[1] = average_current
    elif (allow_thinning is True) and (photo_current[1] > photo_current[2]):
        average_current = np.average(photo_current[1:3])
        if photo_current[0] > average_current:
            average_current = np.average(photo_current[:3])
            photo_current[0] = average_current
        photo_current[1] = average_current
        photo_current[2] = average_current
    elif (allow_thinning is True) and (photo_current[2] > photo_current[3]):
        average_current = np.average(photo_current[2:])
        if photo_current[1] > average_current:
            average_current = np.average(photo_current[1:])
            photo_current[1] = average_current
        photo_current[2] = average_current
        photo_current[3] = average_current
    min_photo_current = min(photo_current)
    if db is False:
        if Eg4 < 2*voltage_loss:
            U_oc = (Eg1 + Eg2 + Eg3 + Eg4/2 - 3*voltage_loss)
        else:
            U_oc = (Eg1 + Eg2 + Eg3 + Eg4 - 4*voltage_loss)
    else:
        U1 = Voc_det_balance(Eg1, spectrum, max_gap, nid=nid,
                    photo_current=photo_current[0])
        U2 = Voc_det_balance(Eg2, spectrum, Eg1,
                nid=nid, photo_current=photo_current[1])
        U3 = Voc_det_balance(Eg3, spectrum, Eg2,
                nid=nid, photo_current=photo_current[2])
        U4 = Voc_det_balance(Eg4, spectrum, Eg3,
                nid=nid, photo_current=photo_current[2])
        U_oc = U1 + U2 + U3 + U4
    result = np.zeros((1), dtype=[('E0', float),('gaps',float,(1,4)),('j',float),('P',float)])
    result['gaps'] = [Eg1, Eg2, Eg3, Eg4]
    result['E0'] = E0
    if power_E0 is True:
        current = match_cats_diode_current(E0, min_photo_current, U_oc, cat_para, nid)
        power = current*E0
        print('STF efficiency (%) with bandgaps Eg1, Eg2, Eg3, Eg4 =', Eg1,
              ',', Eg2, ',', Eg3, ',', Eg4, 'eV for E0=', E0, 'eV:', power*100*etaf_local,
              ';\nCurrent (A/cm^2): ', current)
    else:
        [power, U_mpp, current] = mpp_single_diode_current(min_photo_current,
            U_oc, nid, temp=300)
        print('Photovoltaic efficiency (%) with bandgaps Eg1, Eg2, Eg3, Eg4 =',
              Eg1, ',', Eg2, ',', Eg3, ',', Eg4, 'eV:', power*100*etaf_local,
              ';\nCurrent (A/cm^2): ', current, '.\n')
    result['P'] = power
    result['j'] = current
    return result


# solar to carbon efficiency

def current_to_stc(spectrum, current_density, el_per_molecule=2, C=1.):
    """Calculates the maximum solar-to-carbon (STC) efficiency following the
    ideas of DOI:10.5194/esd-2018-53. Done by taking the current of an STF
    calculation and post-processing this.

    Parameters
    ----------
    el_per_molecule: integer
        How many electrons are required for the reduction of one CO2 molecule?
        Default: 2, which would be suitable for formate. Oxalate would be 1.
    C: float
        Light concentration factor.

    Returns
    -------
    STC: numpy.array
        The solar-to-carbon efficiency in per cent.
    """

    conv_je_STC = 1/(el_per_molecule*spectrum.total_flux*C)
    STC = np.array(current_density, copy=True)*conv_je_STC*100  # in per cent
    return(STC)

# demo routines


def demo_single_junction(data=am15g, db=True):
    """Demo for single junction with AM 1.5G spectrum and IrO2 anode with 1 Ohm
    resistivity.
    """
    print('You should see a plot soon.')
    catparameters = deepcopy(cat_odrop)
    catparameters[2] = 1
    max_power_single_E0(data, cat_para=catparameters, plotting=True, db=db)
    plt.show()


def demo_double_junction(data=am15g, thinning=False, stepsize=0.2, db=True):
    """Demo for double junction with AM 1.5G spectrum and IrO2 anode with 1 Ohm
    resistivity.
    """
    print('Calculating Solar-to-Fuel effiency for DeltaG from 1.0 to 3.0:\n',
          flush=True)
    print('You should see a plot after the end of the calculation.')
    catparameters = deepcopy(cat_odrop)
    catparameters[2] = 1
    max_power_double_E0(data, stepsize=stepsize, cat_para=catparameters,
                        allow_thinning=thinning, plotting=True, db=db)


def demo_triple_junction(data=am15g, thinning=False, stepsize=0.5, db=True):
    """Demo for triple junction with AM 1.5G spectrum and IrO2 anode with 1 Ohm
    resistivity.
    """
    print('Calculating Solar-to-Fuel effiency for DeltaG from 2.0 to 4.0:\n'
          '(This will take some time.)\n', flush=True)
    print('You should see a plot after the end of the calculation.')
    catparameters = deepcopy(cat_odrop)
    catparameters[2] = 1
    max_power_triple_E0(data, min_E0=2.0, max_E0=4.1, stepsize=stepsize,
                        cat_para=catparameters,
                        allow_thinning=thinning, plotting=True, db=db)


def demo_quadruple_junction(data=am15g, thinning=False, stepsize=1.0, db=True):
    """Demo for quadruple junction with AM 1.5G spectrum and IrO2 anode with
    1 Ohm resistivity.
    """
    print('Calculating Solar-to-Fuel effiency for DeltaG from 3.0 to 5.0:\n'
          'This will take a loong time '
    '(up to hours)!\n', flush=True)
    print('You should see a plot after the end of the calculation.')
    catparameters = deepcopy(cat_odrop)
    catparameters[2] = 1
    max_power_quadruple_E0(am15g, min_E0=3.0, max_E0=5.1, stepsize=stepsize,
        cat_para=catparameters, allow_thinning=thinning, plotting=True,
        db=db)


def demo_cat_performance(data=am15g, db=True, step_t=2e-2, drop=2, step_e=5):
    """ Demo for plotting the dual-junction tandem efficiency as a function of
    exchange current density and Tafel slope.
    """
    print('Calculating tandem efficiencies.\n(This will take some time.)\n',
          flush=True)
    ecds = np.logspace(-13, -6, num=step_e)
    tslopes = np.arange(5.e-3, 0.165, step_t)
    efficiencies = np.zeros([len(tslopes), len(ecds)])
    for slope in tslopes:
        i = np.where(tslopes[:] >= slope)[0][0]
        for ecd in ecds:
            j = np.where(ecds[:] >= ecd)[0][0]
            tmp = find_max_double(data, cat_para=[ecd, slope, drop,
                                  cat_odrop[3], cat_odrop[4]], db=db)[1]
            print(i, j, flush=True)
            print(tmp[-1], flush=True)
            efficiencies[i, j] = tmp[-1]
    ecds_log = []
    for ecd in ecds:
        # ecds in mAcm^-2:
        ecds_log.append(-np.log10(ecd*1000))
    fig, axes = plt.subplots(nrows=1,ncols=1)
    p1 = axes.pcolormesh(ecds_log, tslopes*1000, efficiencies*1000, cmap=plt.cm.jet,
                     vmin=abs(efficiencies*1000).min(), vmax=abs(efficiencies*1000).max(),shading='gouraud')
    axes.set_xlabel("-log$_{10}$(Exchange current density / mA$\cdot$cm$^{-2}$)", fontweight='bold', fontsize=14)
    axes.set_ylabel("Tafel slope / mV$\cdot$dec$^{-1}$", fontweight='bold', fontsize=14)
    axes.tick_params(color='black')
    plt.ylim([5, 155])
    fig.subplots_adjust(right=0.85)
    cbar_ax = fig.add_axes([0.9, 0.15, 0.03, 0.7])
    cb = fig.colorbar(p1, cax=cbar_ax)
    cb.set_label('STH / %')
    plt.show()


if __name__ == "__main__":
    # argparse options
    help_text = """YaSoFo directly from the command line. Not all options
    available, yet."""
    parser = argparse.ArgumentParser(description=help_text,
                            formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-b', '--etadouble', action='store_true',
                        help='Plot efficiency over gaps for double junction.')
    parser.add_argument('-d', '--demo', action='store_true', help='Run Demo.')
    parser.add_argument('-e', '--eload', dest='E0', type=float, default=1.23,
                        metavar='E0', help='Electrochemical load. Default: 1.23.')
    parser.add_argument('-ndb', '--nodetbal', action='store_false',
                        help='No detailed balance!')
    parser.add_argument('-nj', '--junctions', dest='nj', type=int, default=2,
                        metavar='j', help='Number of junctions. Default: 2.')
    parser.add_argument('-nt', '--nothinning', action='store_false',
                        help='No thinning!')
    parser.add_argument('-pv', '--photovoltaic', action='store_false',
                        help='Photovoltaic mode, no E0. Not for demo.')
    parser.add_argument('-f', '--file', dest='filename', type=str, default=None,
                        metavar='filename', help='Filename for spectrum. Default: am1.5g.dat')

    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    if args.demo:
        if args.photovoltaic is False:
            print('No demo for photovoltaic mode. Try option -b.')
            sys.exit(1)
        if args.nj == 1:
            print('\nDemo for single junction.\n')
            demo_single_junction(db=args.nodetbal)
        elif args.nj == 2:
            print('\nDemo for double junction.\n')
            demo_double_junction(db=args.nodetbal, thinning=args.nothinning)
        elif args.nj == 3:
            print('\nDemo for triple junction.\n')
            demo_triple_junction(db=args.nodetbal, thinning=args.nothinning)
        elif args.nj == 4:
            print('\nDemo for quadruple junction.\n')
            demo_quadruple_junction(db=args.nodetbal, thinning=args.nothinning)
    elif args.etadouble:
        if args.photovoltaic is True:
            print('\nCalculating efficiency over bandgaps for E0=%2.2f eV.\n'%args.E0)
        else:
            print('\nCalculating efficiency over bandgaps.\n')
        print('Efficiency scaled to integrated total power of the spectrum.')
        if args.filename is not None:
            print('Using spectrum file %s.' % args.filename)
            spectrum = solspec(args.filename)
            best_gaps_double(spectrum=spectrum, power_E0=args.photovoltaic,
                             E0=args.E0, db=args.nodetbal,
                             allow_thinning=args.nothinning, etascale=True)
        else:
            best_gaps_double(power_E0=args.photovoltaic, E0=args.E0,
                             db=args.nodetbal, allow_thinning=args.nothinning,
                             etascale=True)
