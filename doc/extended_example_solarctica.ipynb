{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## **YaSoFo Solarctica example**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example, we show how to do a series of calculations using the Solarctica functions. The system will be a double junction and very good catalysts for both the oxygen and the hydrogen evolution reaction. We calculate the Solar-To-Hydrogen (STH) efficiency for a thermally decoupled and coupled device while varying the outdoor temperature as well as the area-ratio of the catalyst and solar cells. In the end, we will plot the results and show the efficiency benefits from thermal coupling. For further features and more detailed explanations see the documentation of the Solarctica functions. Prerequisites: You have installed [SciPy](https://scipy.org/) (Python 3.x version) on your computer and obtained YaSoFo from https://bitbucket.org/YaSoFo/yasofo . You have started a Python 3 shell, such as IPython, in the directory where the file `yasofo.py` is located. (Starting this from the notebook, we assume it to be located in the `doc` folder of YaSoFo.)\n",
    "\n",
    "The calculations are similar to those published in M. Kölbach, K. Rehfeld, and M. M. May. *Efficiency Gains for Thermally Coupled Solar Hydrogen Production in Extreme Cold*, Energy & Environmental Science (2021), accepted for publication. [DOI:10.1039/d1ee00650a](htps://doi.org/10.1039/d1ee00650a)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Spectrum successfully imported. Length: 120 points.\n",
      "Integrated power: 1000.000 W/m^2. Expected: 1000 W/m^2.\n",
      "Standard for efficiency calculation assumes 1000 W/m^2!\n",
      "Length: 120 points. Recommended: 200-500.\n"
     ]
    }
   ],
   "source": [
    "import sys\n",
    "# change to yasofo root dir for the case, that this notebook is in the doc-dir:\n",
    "sys.path.append('../')\n",
    "\n",
    "from src.solarctica import estimate_JV_from_datasheet, get_STH_eff, calc_device_t, itertation_func_general, match_catalysts_solar_cell_current\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.cm as cm\n",
    "import matplotlib.colors as colors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## **1) Input parameters**\n",
    "\n",
    "**1.1) Solar cell parameters:** <br>\n",
    "> - *C*: Light concentration factor [ ] <br>\n",
    "> - *Isc_Tref*: Short current density at T_ref from the datasheet [mA/cm²] <br>\n",
    "> - *Isc_T_coeff*: Temperature coefficient of the short current density [%/K] <br>\n",
    "> - *U_oc_Tref*: Open circiut potential at T_ref from the datasheet [V] <br>\n",
    "> - *U_oc_T_coeff*: Temperature coefficient of the open circiut potential [%/K] <br>\n",
    "> - *T_ref*: Reference temperature of values from the datasheet [K]<br>\n",
    "> - *nid*: Diode ideality factor [ ]; can be estimated from the slope of V_oc vs. log(C) <br>\n",
    "\n",
    "\n",
    " **1.2) Catalyst parameters:** <br> \n",
    "> - *OER_catalyst_para*: List of the following four values\n",
    ">> j0_OER_Tref: exchange current density [A/cm²] <br> \n",
    ">> Tref_OER: Reference temperature of the value of jo_OER_Tref [K] <br> \n",
    ">> Ea_OER: Activation Energy OER [J/M] <br> \n",
    ">> Alpha_a_OER: Anodic Charge transfer coefficient multiplied with electrons involved (can be extracted from the tafel slope [ ] <br> \n",
    "           \n",
    "> - *HER_catalyst_para*: List of the following four values\n",
    ">> j0_HER_Tref: exchange current density [A/cm²] <br> \n",
    ">> Tref_HER: Reference temperature of the value of jo_OER_Tref [K] <br> \n",
    ">> Ea_HER: Activation Energy OER [J/M] <br> \n",
    ">> Alpha_a_HER: Cathodic charge transfer coefficient multiplied with electrons involved (can be extracted from the tafel slope [ ] <br> \n",
    "\n",
    "                        \n",
    "> - *Electrolyte_para*: List of the following three values  \n",
    ">> pH: pH value of electrolyte [ ] <br> \n",
    ">> y0: y-intercept of the temperature-dependent electrolyte conductivity according to y0 [S/cm] + m [S/(cm$\\cdot$K)]$\\cdot$T [K] <br> \n",
    ">> m: slope (see above)\n",
    "\n",
    "> - *Configuration_para*: List of the following four values <br> \n",
    ">> area_solar_cell: Area of solar cell [m²] <br> \n",
    ">> area_ratio: Ratio of the catalysts and solar cell area [ ] <br> \n",
    ">> distance: distance of electrodes [cm] <br> \n",
    ">> geometry_const: geometry constant of the cell accounting for fringing effects with respect to the iR-drop in the electrolyte [ ] \n",
    "\n",
    " **1.3) Thermal model parameters:** <br>\n",
    "> - *T_out*: Outdoor temperature [K] <br>\n",
    "> - *t*: Transmissivity of the optical collector [ ] <br>\n",
    "> - *abso*: Surface absorptivity of the solar cell [ ] (*abso*$\\cdot$*t* = transmissitivty of the optical train)<br>\n",
    "> - *area_solar_cells_total*: Total area of the solar cells [m²] <br>\n",
    "> - *I0*: Irradiance [W/m²] (1000 W/m² for AM1.5G)<br>\n",
    "> - *e_solarcell*: Emmisivity of the solar cells [ ]<br>\n",
    "> - *area_housing*: Area of the device housing [m²] <br>\n",
    "> - *e_housing*: Emmisivity of the device housing [ ] <br>\n",
    "> - *h_solarcell_air*: Convective transfer coefficient solar cell - air [W/(m²$\\cdot$K)] <br>\n",
    "> - *h_housing_air*: Convective transfer coefficient device housing - air [W/(m²$\\cdot$K)] <br>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Solar cell parameters\n",
    "C = 1\n",
    "Isc_Tref = 17\n",
    "Isc_T_coeff = 0.08\n",
    "U_oc_Tref = 1.88\n",
    "U_oc_T_coeff = -0.21\n",
    "T_ref = 298.15\n",
    "nid = 2\n",
    "\n",
    "#Catalyst parameters\n",
    "OER_catalyst_para = [1.86e-9, 323, 52000, 1.5]\n",
    "HER_catalyst_para = [0.68e-3, 303, 13200, 1.2]\n",
    "Electrolyte_para = [0, -2.68611, 0.01176]\n",
    "Configuration_para = [0.0007, 0.5, 1, 0.3]\n",
    "\n",
    "#Thermal model\n",
    "t = 1\n",
    "abso = 0.9\n",
    "I0 = 1000\n",
    "e_solarcell = 0.9\n",
    "area_housing = 0.00175\n",
    "e_housing = 0.9\n",
    "h_solarcell_air = 10\n",
    "h_housing_air = 10\n",
    "\n",
    "#Iteration parameter\n",
    "max_iter = 100\n",
    "delta = 0.0001\n",
    "j_op_guess = 150 #[A/cm²]\n",
    "E_op_guess = 2  #[V]\n",
    "\n",
    "#This defines the resolution of your calculations (in both x and y):\n",
    "Resolution = 8\n",
    "\n",
    "#Temperature range:\n",
    "T_out_min = 253.15 \n",
    "T_out_max = 298.15 \n",
    "T_number_of_points = Resolution\n",
    "Temperature_range = np.linspace(T_out_min, T_out_max, T_number_of_points)\n",
    "\n",
    "#Area-ratio range:\n",
    "area_ratio_min = 0.01\n",
    "area_ratio_max = 1\n",
    "area_ratio_number_of_points = Resolution\n",
    "area_ratio_range = np.linspace(area_ratio_min, area_ratio_max, area_ratio_number_of_points)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## **2) Calculations for a thermally decoupled device**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "STH_efficiencys_area_ratio = []\n",
    "Configuration_para_loop_decoupled = Configuration_para.copy()\n",
    "\n",
    "for T in Temperature_range:\n",
    "    for Configuration_para_loop_decoupled[1] in area_ratio_range:\n",
    "        \n",
    "        STH_area_ratio = get_STH_eff(T, OER_catalyst_para, HER_catalyst_para, \n",
    "                      Electrolyte_para, Configuration_para_loop_decoupled, C,\n",
    "                      T_ref = T_ref,  Isc_Tref = Isc_Tref, Isc_T_coeff = Isc_T_coeff, \n",
    "                      nid = nid, U_oc_Tref = U_oc_Tref, U_oc_T_coeff = U_oc_T_coeff, \n",
    "                      method = 'datasheet', thermal_coupling = False, \n",
    "                      max_iter = max_iter, delta = delta, j_op_guess = j_op_guess, \n",
    "                      E_op_guess = E_op_guess, t = t, abso = abso,I0 = I0, e_solarcell = e_solarcell, \n",
    "                      area_housing = area_housing,\n",
    "                      e_housing = e_housing , h_solarcell_air = h_solarcell_air, \n",
    "                      h_housing_air = h_housing_air)\n",
    "        \n",
    "        STH_efficiencys_area_ratio.append(STH_area_ratio['STH'])\n",
    "\n",
    "STH_efficiencys_area_ratio_matrix = [STH_efficiencys_area_ratio[i:i+len(area_ratio_range)] for i in range(0, len(STH_efficiencys_area_ratio), len(area_ratio_range))][::-1]\n",
    "print('Done')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## **3) Calculations for a thermally coupled device**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "STH_efficiencys_area_ratio_coupled= []\n",
    "Configuration_para_loop_coupled= Configuration_para.copy()\n",
    "\n",
    "for T in Temperature_range:\n",
    "    for Configuration_para_loop_coupled[1] in area_ratio_range:\n",
    "\n",
    "        STH_area_ratio_coupled = get_STH_eff(T, OER_catalyst_para, HER_catalyst_para, \n",
    "                              Electrolyte_para, Configuration_para_loop_coupled, C,\n",
    "                              T_ref = T_ref,  Isc_Tref = Isc_Tref, Isc_T_coeff = Isc_T_coeff, \n",
    "                              nid = nid, U_oc_Tref = U_oc_Tref, U_oc_T_coeff = U_oc_T_coeff, \n",
    "                              method = 'datasheet',thermal_coupling = True, max_iter = max_iter,\n",
    "                              delta = delta, j_op_guess = j_op_guess, E_op_guess = E_op_guess,\n",
    "                              t = t, abso = abso, I0 = I0, e_solarcell = e_solarcell, \n",
    "                              area_housing = area_housing, e_housing =e_housing, h_solarcell_air =h_solarcell_air, \n",
    "                              h_housing_air = h_housing_air)\n",
    "\n",
    "        STH_efficiencys_area_ratio_coupled.append(STH_area_ratio_coupled['STH'])\n",
    "        \n",
    "Difference_STH_efficiencys_area_ratio_coupled = np.array(STH_efficiencys_area_ratio_coupled) - np.array(STH_efficiencys_area_ratio)\n",
    "Difference_STH_efficiencys_area_ratio_coupled_matrix = [Difference_STH_efficiencys_area_ratio_coupled[i:i+len(area_ratio_range)] for i in range(0, len(Difference_STH_efficiencys_area_ratio_coupled), len(area_ratio_range))]\n",
    "Difference_STH_efficiencys_area_ratio_coupled_matrix_np = np.array(Difference_STH_efficiencys_area_ratio_coupled_matrix, dtype=float)[::-1]\n",
    "print('Done')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##  **4) Let's plot the data**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig,axes = plt.subplots( ncols=1, nrows=2, figsize=(7, 8), gridspec_kw={'width_ratios':[1], 'height_ratios':[1,1]})\n",
    "\n",
    "labelsize_tick = 19\n",
    "fontsize_anno = 18\n",
    "fontsize_label = 24\n",
    "\n",
    "def forceAspect(ax,aspect=1):\n",
    "    im = ax.get_images()\n",
    "    extent =  im[0].get_extent()\n",
    "    ax.set_aspect(abs((extent[1]-extent[0])/(extent[3]-extent[2]))/aspect)\n",
    "    \n",
    "aspect_subplots = 1.5\n",
    "Colorbar_range_min = 0\n",
    "Colorbar_range_max = 20\n",
    "Colorbar_range_min_difference = -3.2\n",
    "Colorbar_range_max_difference = 7.2\n",
    "# divnorm = colors.DivergingNorm(vmin=Colorbar_range_min_difference, vcenter=0, vmax=Colorbar_range_max_difference) # commenting out as deprecated\n",
    "bbox_props = dict(boxstyle=\"round\", fc=\"white\", lw=0.5, alpha=.9)\n",
    "levels_STH = np. array([5, 10, 15, 18, 20] , dtype=float)\n",
    "levels_deltaSTH = np. array([0,1,3,4,5,6] , dtype=float)\n",
    "\n",
    "# Thermally decoupled \n",
    "im1 = axes[0].imshow(STH_efficiencys_area_ratio_matrix, interpolation='bilinear', cmap = cm.get_cmap('RdYlBu_r'), extent=[area_ratio_min, area_ratio_max, T_out_min-273.15, T_out_max-273.15],  vmin=Colorbar_range_min, vmax=Colorbar_range_max)\n",
    "CS1 = axes[0].contour(STH_efficiencys_area_ratio_matrix, levels_STH, colors='black', extent=[area_ratio_min, area_ratio_max,  T_out_max-273.15, T_out_min-273.15])\n",
    "axes[0].clabel(CS1, inline=2, fontsize=15.5, fmt='%1.1f')\n",
    "axes[0].tick_params(axis='x', labelsize = labelsize_tick, labelbottom=False)\n",
    "axes[0].tick_params(axis='y', labelsize = labelsize_tick)\n",
    "axes[0].set_ylabel(\"$T_{Outdoor}$ (°C)\", fontsize=fontsize_label)\n",
    "forceAspect(axes[0], aspect=aspect_subplots)\n",
    "t = axes[0].text(0.665, 19.6, 'decoupled', size=fontsize_anno,weight = 'bold', bbox=bbox_props)\n",
    "\n",
    "# Efficiency gains for the thermally coupled configuration\n",
    "im2 = axes[1].imshow(Difference_STH_efficiencys_area_ratio_coupled_matrix_np, interpolation='bilinear', cmap = cm.get_cmap('coolwarm'), extent=[area_ratio_min, area_ratio_max, T_out_min-273.15, T_out_max-273.15],  vmin=Colorbar_range_min_difference, vmax=Colorbar_range_max_difference)\n",
    "CS2 = axes[1].contour(Difference_STH_efficiencys_area_ratio_coupled_matrix_np, levels_deltaSTH, colors='black', extent=[area_ratio_min, area_ratio_max, T_out_max-273.15, T_out_min-273.15])\n",
    "axes[1].clabel(CS2, inline=1, fontsize=15.5, fmt='%1.1f')\n",
    "axes[1].set_ylabel(\"$T_{Outdoor}$ (°C)\", fontsize=fontsize_label)\n",
    "axes[1].tick_params(axis='x',  labelsize = labelsize_tick)\n",
    "axes[1].tick_params(axis='y', labelsize = labelsize_tick)\n",
    "axes[1].set_xlabel(\" $A_{catalysts}$/$A_{solar~cells}$-ratio \", fontsize=fontsize_label, labelpad = 10)\n",
    "forceAspect(axes[1], aspect=aspect_subplots)\n",
    "t = axes[1].text(0.445, 19.5, 'thermally coupled', size=fontsize_anno,weight = 'bold', bbox=bbox_props)\n",
    "plt.tight_layout()\n",
    "\n",
    "# Colorbars\n",
    "fig.subplots_adjust(right=0.95)\n",
    "\n",
    "cbar_ax = fig.add_axes([0.92, 0.577, 0.06, 0.405])\n",
    "cbar = plt.colorbar(im1, cax=cbar_ax,  orientation=\"vertical\")\n",
    "cbar.set_label(label='$\\\\eta_{STH}$ (%)', size= fontsize_label)\n",
    "cbar.ax.tick_params(labelsize=labelsize_tick)\n",
    "\n",
    "cbar_ax_2 = fig.add_axes([0.92, 0.135, 0.06, 0.405])\n",
    "cbar2 = plt.colorbar(im2, cax=cbar_ax_2,  orientation=\"vertical\")\n",
    "cbar2.set_label(label='$\\Delta$ $\\\\eta_{STH}$ (%)', size= fontsize_label)\n",
    "cbar2.ax.tick_params(labelsize=labelsize_tick)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
